<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {

            if(count($request->segments())>1 && auth('admin')->user()->rule_id != 1 ){
                if(!adminHasPermissions($request->segment(2))){
                    abort(404);
                }
            }
            return $next($request);
        }else{
            return redirect('admin/login');
        }


    }
}
