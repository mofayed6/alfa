<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\UserActivation;
use Illuminate\Http\Request;
use Modules\User\Models\User;

class AuthController extends Controller
{
    public function login()
    {
        return View('front.auth.login');
    }
    public function doLogin(Request $request)
    {
        $this->validate($request,[
            'email'     => 'required|email',
            'password'  => 'required|min:6',
        ]);
        $user = User::where('email',$request->email)->first();
        if(!$user){
            return back()->withErrors(['email'=>'user not exist'])->withInput($request->all());
        }
        if(auth()->attempt($request->only(['email','password']),$request->rememberme)){
            return redirect(url('/'));
        }
    }

    public function register()
    {
        return View('front.auth.register');
    }
    public function doRegister(Request $request)
    {
        $this->validate($request,[
            'first_name'=>'required',
            'last_name'=>'required',
            'phone_number'=>'required',
            'email'=>'required|unique:users',
            'password'=>'required|min:6',
            'gender'=>'required',
        ]);
        $user = new User();
        $user->first_name       = $request->first_name;
        $user->last_name        = $request->first_name;
        $user->mobile           = $request->phone_number;
        $user->email            = $request->email;
        $user->password         = bcrypt($request->password);
        $user->gender           = $request->gender;
        $user->activation_code  = rand(100000,999999);
        $user->status           = 0;
        $user->save();
        $mail = \Mail::to($user)->send(new UserActivation($user));
        return redirect(url('email/verify/'.$user->email))->with(['success'=>'mail sent']);
    }

    public function logout()
    {
        if(auth()->check()){
            \Auth::logout();
        }
        return redirect(url('/'));
    }
}
