<?php

namespace App\Http\Controllers\Auth;

use App\Mail\UserActivation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\User\Models\User;

class VerifyController extends Controller
{
    public function verifyPage($email)
    {
        $user = User::where('email',$email)->first();
        if($user->status != 0){
            return View('front.auth.verified');
        }
        return View('front.auth.verify');
    }

    public function manualVerify($email,Request $request)
    {
        return $this->verify($email,$request->token);
    }

    public function verify($email,$token)
    {
        $user = User::where('email',$email)->first();
        if(!$user){
            dd('user not found');
        }
        if($user->status != 0){
            return redirect(url('email/verify/'.$email))->withErrors(['email'=>'user doesn\'t need activation']);
        }
        if($user->activation_code != $token){
            return redirect(url('email/verify/'.$email))->withErrors(['email'=>'invalid token']);
        }
        $user->activation_code = null;
        $user->status = 1;
        $user->save();

        auth('web')->login($user);
        return redirect(url('/'));
    }

    public function resendVerifyToken($email)
    {
        $user = User::whereEmail($email)->first();
        if(!$user){
            return redirect(url('email/verify/'.$email))->withErrors(['email'=>'User Not Found']);
        }
        if($user->status != 0){
            return redirect(url('email/verify/'.$email))->withErrors(['email'=>'user doesn\'t need activation']);
        }
        $user->activation_code  = rand(100000,999999);
        $user->save();
        $mail = \Mail::to($user)->send(new UserActivation($user));
        return redirect(url('email/verify/'.$email))->with(['resent'=>'Verification Mail Resent']);
    }

}
