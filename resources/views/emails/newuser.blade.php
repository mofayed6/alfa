@component('mail::message')
# Hello, {{$user['first_name']}}

Welcome to Alfamarket,
Please Click the button below to activate your account

@component('mail::button', ['url' => url('email/verify/'.$user->email.'/'.$user->activation_code)])
Activate Account
@endcomponent

Or Enter this code Manually : <b><code>{{$user->activation_code}}</code></b>

Thanks,<br>
Alfamarket
@endcomponent
