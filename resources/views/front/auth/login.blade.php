
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="robots" content="index, follow">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="The initial Workflow of Ahmed Shaarawy">
    <title>Arafa Group</title>
    <!--<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/touch-icon-iphone.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="images/touch-icon-ipad.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="images/touch-icon-iphone-retina.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="images/touch-icon-ipad-retina.png" />
    <link rel="mask-icon" href="images/safari-pinned-tab.svg" color="#ff2857" />-->
    <link rel="stylesheet" href="{{Url('front/')}}/css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Cairo:300,400,600" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <![endif]-->
</head>

<body>
<div class="globalWrapper">
    <div class="welcome">
        <div class="logo">
            <a href="index.html">
                <img src="images/logo.svg" alt="">
            </a>
        </div>
        <div class="form_box">
            <div class="title">
                <h1>Log In</h1>
                <a href="{{Url('register')}}" class="colored">Sign Up</a>
            </div>
            {{Form::open()}}
                <div class="label_top">
                    <div class="custom_field">
                        <label for="">Email</label>
                        {{Form::email('email',null,['class'=>'custom_input'])}}
                        <small class="text-danger">{{$errors->first('email')}}</small>
                    </div>
                    <div class="custom_field">
                        <label for="">Password</label>
                        {{Form::password('password',['class'=>'custom_input'])}}
                        <small class="text-danger">{{$errors->first('password')}}</small>

                    </div>
                    <div class="checkbox inline no_margin">
                        {{Form::checkbox('rememberme',1,null,['id'=>'remember'])}}
                        <label for="remember">Remember Me</label>
                    </div>
                    <a href="#" class="colored pull-right">Forgot Password?</a>
                    <button type="submit" class="btn btn-primary btn-block m_t_20 shadow_hover">Log In</button>
                </div>
            {{Form::close()}}
            <div class="sign_in_with">
                <div class="or"><span>Or</span></div>
                <a href="#"><img src="https://cf2.s3.souqcdn.com/public/style/img/en/facebook-login-button-new.png" alt=""></a>
            </div>
            <div class="to_signup">
                <p>Don't have an account?</p>
                <a href="#" class="btn btn-basic btn-block">Sign Up</a>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <p><strong>© 2018 Arafa Group</strong></p>
                <ul class="list_inline colored">
                    <li><a href="https://egypt.souq.com/eg-en/about-us/c/">About Us</a></li>
                    <li><a href="http://pr.souq.com/">Media Center</a></li>
                    <li><a href="https://egypt.souq.com/eg-en/why-souq/c/">Careers</a></li>
                    <li><a href="https://egypt.souq.com/eg-en/privacy-policy/c/">Privacy Policy</a></li>
                    <li><a href="https://egypt.souq.com/eg-en/terms-and-conditions/c/">Terms and Conditions</a></li>
                    <li><a href="https://affiliates.souq.com/">Affiliate Program</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
