@extends('front.layouts.master')
@section('content')

    <div class="banner">
        <div class="container">
            <div class="slider_container">
                <div class="slider">
                    <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
                    <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
                    <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
                </div>
            </div>
            <div class="ads">
                <div class="ad1">
                    <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
                </div>
                <div class="ad2">
                    <a href="#"><img src="{{Url('front/')}}/images/banner_img.jpg" alt="" class=""></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner -->

    <div class="promo-boxes">
        <div class="container">
            <div class="user-links">
                <div class="user-info">
                    <a href="#!"><img src="{{Url('front/')}}/images/avatar-placeholder.png" alt="User Avatar"></a>
                    <div>
                        <h5>Hi, Shaker</h5>
                        <p>Customer since 2014</p>
                    </div>
                </div>

                <span>Top links for you</span>

                <ul>
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/promo/box.png" alt="Promo Image">
                            <p>Your Orders</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/promo/headphone.png" alt="Promo Image">
                            <p>Electronics</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/promo/controller.png" alt="Promo Image">
                            <p>Video Games</p>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{Url('front/')}}/images/promo/game.png" alt="Promo Image">
                            <p>Apps & Games</p>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="recent">
                <h5>Recently viewed</h5>

                <figure>
                    <img src="{{Url('front/')}}/images/promo/dod.jpg" alt="Deal of the day image">
                </figure>

                <p>Sat, Sep 08</p>

                <div class="cta">
                    <a href="#!">See your browsing history</a>
                </div>
            </div>

            <div class="dod">
                <h5>Deal of the Day</h5>

                <figure>
                    <img src="{{Url('front/')}}/images/promo/dod.jpg" alt="Deal of the day image">
                </figure>

                <div class="price">
                    <p><span>$</span>27<span>74</span> - <span>$</span>33<span>19</span></p>
                </div>

                <p>Save 25% on lospure Protein Powders</p>

                <div class="cta">
                    <a href="#!">See more deals</a>
                </div>
            </div>


            <div class="offer">
                <h5>Buy 2 and save $10</h5>

                <figure>
                    <img src="{{Url('front/')}}/images/promo/offer.jpg" alt="Deal of the day image">
                </figure>

                <p>All-new Echo Dot. Discount reflected in cart. Limited-time offer.</p>

                <div class="cta">
                    <a href="#!">Learn more</a>
                </div>
            </div>
        </div>
    </div>
    <!-- promo-boxes -->

    <!-- <div class="home_categories block m_t_30">
        <div class="container">
            <h2>Featured Categories</h2>
            <div class="categories carousel">
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/polo-shirt.svg" alt="">
                        <h4>Clothing</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/cream.svg" alt="">
                        <h4>Beauty Gift Sets</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/fridge.svg" alt="">
                        <h4>Appliances</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/couch.svg" alt="">
                        <h4>Home Furniture</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/smart-tv.svg" alt="">
                        <h4>Electronics</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/scale.svg" alt="">
                        <h4>Electronics</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/sport-bottle.svg" alt="">
                        <h4>Sports</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/shaver.svg" alt="">
                        <h4>Personal Care</h4>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/boy.svg" alt="">
                        <h4>Kids Wear</h4>
                    </a>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Home Categories -->

    <!-- <div class="big">
        <div class="container">
            <div>
                <a href="#"><img src="{{Url('front/')}}/images/big.jpg" alt=""></a>
            </div>
            <div>
                <a href="#"><img src="{{Url('front/')}}/images/big.jpg" alt=""></a>
            </div>
            <div>
                <a href="#"><img src="{{Url('front/')}}/images/big.jpg" alt=""></a>
            </div>
        </div>
    </div> -->
    <!-- Big Ads -->

    <div class="big-promo">
        <div class="container">
            <a href="#"><img src="{{Url('front/')}}/images/promo/big-promo.jpg" alt="Big Promo Image"></a>
        </div>
    </div>

    <div class="long">
        <div class="container">
            <a href="#"><img src="{{Url('front/')}}/images/long.jpg" alt=""></a>
        </div>
    </div>
    <!-- Full Width Ad -->

    <div class="home_offers block">
        <div class="container">
            <div class="offers">
                <div class="v2">
                    <a href="#">
                        <img src="{{Url('front/')}}/images/offer.jpg" alt="">
                        <p>Save 30% on your next pair of jeans</p>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/offer.jpg" alt="">
                        <p>Save 30% on your next pair of jeans</p>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <img src="{{Url('front/')}}/images/offer.jpg" alt="">
                        <p>Save 30% on your next pair of jeans</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Home Offers  -->

    <!-- <div class="long m_t_30">
        <div class="container">
            <a href="#"><img src="{{Url('front/')}}/images/long.jpg" alt=""></a>
        </div>
    </div> -->
    <!-- Full Width Ad -->
    @if(count($latestProducts)>0)
        <div class="latest_products block m_t_30">
            <div class="container">
                <h2>Latest Products</h2>
                <div class="products carousel">
                    @foreach($latestProducts as $product)
                        <div>
                            <a href="#">
                                <div class="badge blue"><span>o</span></div>
                                <div class="badge"><span>g</span></div>
                                <figure>
                                    <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                                </figure>
                                <h3>{{$product->name}}</h3>
                                <div class="price">
                                    @if($product->discount_price != $product->price)
                                        <p class="before"><span>{{$product->price}} EGP</span> <span>10% OFF</span></p>
                                    @endif
                                    <p class="after">
                                        <span>{{($product->discount_price != $product->price)?$product->discount_price:$product->price}}</span> EGP
                                        <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart" onclick="return addToCart({{$product->id}})">
                                    </p>
                                </div>
                            </a>
                        </div>
                    @endforeach


            </div>
        </div>
    </div>
    <!-- Latest Products  -->
    @endif

    <div class="trending_products block m_t_30">
        <div class="container">
            <h2>Trending Products</h2>
            <div class="products carousel">
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="#">
                        <div class="badge"><span>33</span></div>
                        <figure>
                            <img src="{{Url('front/')}}/images/J_milk.png" alt="">
                        </figure>
                        <h3>Lactose Free Milk - 1 Liter</h3>
                        <div class="price">
                            <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                            <p class="after"><span>19.99</span> EGP <img src="{{Url('front/')}}/images/add-to-cart.svg" alt="Add To Cart"></p>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>

@stop
