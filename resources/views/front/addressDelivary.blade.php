@extends('front.layouts.master')

@section('style')

   <style type="text/css">
    #map {
        height: 500px;
        width: 100%
      }
   </style>

@endsection

@section('content')

<div class="main_content">
    <div class="container">
        <div class="main_with_sidebar">
            <aside>
                <div class="account_settings">
                    <div class="user">
                        <div class="icon">
                            <i class="mIcon"></i>
                        </div>
                        <div class="details">
                            <h5>Kirollos Magdy</h5>
                            <p>kirollosmagdy17@gmail.com</p>
                        </div>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="#"><i class="mIcon"></i>My Orders</a></li>
                            <li class="active"><a href="#"><i class="mIcon"></i>Shipping Addresses</a></li>
                            <li><a href="#"><i class="mIcon"></i>Wish Lists</a></li>
                            <li><a href="#"><i class="mIcon"></i>Recommended</a></li>
                            <li><a href="#"><i class="mIcon"></i>Account Settings</a></li>
                        </ul>
                    </nav>
                </div>
            </aside>
            <main>
                <div class="title flex vcenter m_b_10">
                    <h4 class="m_t_0">Shipping Addresses</h4>
                    <button type="button" class="btn btn-basic right go_success regular small">Create a new Address</button>
                </div>
                <section class="inner_section white m_t_0">
                    <div class="addresses clearfix">
                        <ul>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="address1" name="addresses" checked>
                                    <label for="address1"><strong>Kirollos Magdy</strong><span class="small space_l grey">(Primary)</span></label>
                                </div>
                                <p class="grey">
                                    Mohandseen
                                    <br> 81
                                    <br> Giza, Egypt
                                    <br> +201000609719
                                </p>
                                <p><a href="#" class="colored">Edit</a></p>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="address2" name="addresses">
                                    <label for="address2"><strong>Kirollos Magdy</strong></label>
                                </div>
                                <p class="grey">
                                    Mohandseen
                                    <br> 81
                                    <br> Giza, Egypt
                                    <br> +201000609719
                                </p>
                                <p><a href="#" class="colored">Edit</a> | <a href="#" class="colored">Delete</a></p>
                            </li>
                        </ul>
                    </div>
                </section>


                <section class="inner_section white m_t_0">
                    <div class="addresses clearfix">
                         {{Form::open(['action'=>['\Modules\DeliveryLocation\Http\Controllers\UserDeliveingController@storeAddress'],'method'=>'post'])}}
                       <!--  <form action="{{url('address-save')}}" method="post">
                            {!! csrf_field() !!} -->
                         <input type="hidden" name="address" id="address" value="">
                         <input type="hidden" name="lat" id="lat" value="">
                         <input type="hidden" name="lng" id="lng" value="">
                         <input id="pac-input" class="form-control" type="text" placeholder="Enter a location">
                         <div id="map"></div>

                         <button type="submit">Save</button>
                        {{Form::close()}}
                      <div id="infowindow-content">

                         

                          <span id="place-name"  class="title"></span><br>
                          <span id="place-id"></span><br>
                         <span id="place-address"></span>
                        </div>
                    </div>




                </section>

            </main>
        </div>
    </div>

@stop


@section('script')
<script type="text/javascript">

 function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });

        var input = document.getElementById('pac-input');

        var autocomplete = new google.maps.places.Autocomplete(
            input, {placeIdOnly: true});
        autocomplete.bindTo('bounds', map);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var geocoder = new google.maps.Geocoder;
        var marker = new google.maps.Marker({
          map: map
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          var place = autocomplete.getPlace();

          if (!place.place_id) {
            return;
          }
          geocoder.geocode({'placeId': place.place_id}, function(results, status) {
             //console.log(results);    
            if (status !== 'OK') {
              window.alert('Geocoder failed due to: ' + status);
              return;
            }
            map.setZoom(11);
            console.log(results[0].geometry.location.lat());
            console.log(results[0].geometry.location.lng());
            map.setCenter(results[0].geometry.location);
            // Set the position of the marker using the place ID and location.
            marker.setPlace({
              placeId: place.place_id,
              location: results[0].geometry.location
            });
            marker.setVisible(true);

            $('#address').val(results[0].formatted_address);
            $('#lat').val(results[0].geometry.location.lat());
            $('#lng').val(results[0].geometry.location.lng());

            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-id'].textContent = place.place_id;
            infowindowContent.children['place-address'].textContent =
                results[0].formatted_address;
            infowindow.open(map, marker);
          });
        });
      }

    </script>

            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoacCf8RDzMIaHcI5Ywh9zQ-kZt1-V_fc&libraries=places&callback=initMap" async defer></script>

@endsection