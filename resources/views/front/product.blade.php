@extends('front.layouts.master')
@section('content')
<div class="main_content">
    <div class="container">
        <div class="breadcrumb">
            <ul class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Electronics</a>
                </li>
                <li>
                    <span>TVs</span>
                </li>
            </ul>
        </div>
        <section class="product_details inner_section white">
            <div class="left" style="margin-top: 0px;">
                <div class="slider_for slick-initialized slick-slider slick-dotted" role="toolbar"><button class="slick-arrow slick-prev slick-disabled" aria-disabled="true" style="display: block;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 40"><g id="down-arrow" transform="translate(-33.4 46.4) rotate(-90)"><path id="Path_140" data-name="Path 140" class="cls-1" d="M45.991,33.786a1.485,1.485,0,0,0-2,0L26.419,50.244,8.812,33.786a1.485,1.485,0,0,0-2,0,1.262,1.262,0,0,0,0,1.868L25.386,53.014a1.43,1.43,0,0,0,1,.386,1.492,1.492,0,0,0,1-.386L45.956,35.654A1.237,1.237,0,0,0,45.991,33.786Z" transform="translate(0 0)"></path></g></svg></button>
                    <div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 1596px; transform: translate3d(0px, 0px, 0px);"><div class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 399px;">
                                <a href="{{url('/')}}/front/images/product.jpg" class="gallery-item" tabindex="0"><img src="{{url('/')}}/front/images/product.jpg" alt=""></a>
                            </div><div class="slick-slide" data-slick-index="1" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide01" style="width: 399px;">
                                <a href="{{url('/')}}/front/images/product.jpg" class="gallery-item" tabindex="-1"><img src="{{url('/')}}/front/images/product.jpg" alt=""></a>
                            </div><div class="slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 399px;">
                                <a href="{{url('/')}}/front/images/product.jpg" class="gallery-item" tabindex="-1"><img src="{{url('/')}}/front/images/product.jpg" alt=""></a>
                            </div><div class="slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03" style="width: 399px;">
                                <a href="{{url('/')}}/front/images/product.jpg" class="gallery-item" tabindex="-1"><img src="{{url('/')}}/front/images/product.jpg" alt=""></a>
                            </div></div></div>



                    <button class="slick-arrow slick-next" style="display: block;" aria-disabled="false"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 40"><g id="down-arrow" transform="translate(-33.4 46.4) rotate(-90)"><path id="Path_140" data-name="Path 140" class="cls-1" d="M45.991,33.786a1.485,1.485,0,0,0-2,0L26.419,50.244,8.812,33.786a1.485,1.485,0,0,0-2,0,1.262,1.262,0,0,0,0,1.868L25.386,53.014a1.43,1.43,0,0,0,1,.386,1.492,1.492,0,0,0,1-.386L45.956,35.654A1.237,1.237,0,0,0,45.991,33.786Z" transform="translate(0 0)"></path></g></svg></button><ul class="slick-dots" style="display: block;" role="tablist"><li class="slick-active" aria-hidden="false" role="presentation" aria-selected="true" aria-controls="navigation00" id="slick-slide00"><button type="button" data-role="none" role="button" tabindex="0">1</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation01" id="slick-slide01"><button type="button" data-role="none" role="button" tabindex="0">2</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation02" id="slick-slide02"><button type="button" data-role="none" role="button" tabindex="0">3</button></li><li aria-hidden="true" role="presentation" aria-selected="false" aria-controls="navigation03" id="slick-slide03"><button type="button" data-role="none" role="button" tabindex="0">4</button></li></ul></div>
                <div class="slider_nav slick-initialized slick-slider">
                    <div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 20000px; transform: translate3d(0px, 0px, 0px);"><img src="{{url('/')}}/front/images/1.jpg" alt="" class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide10"><img src="{{url('/')}}/front/images/2.jpg" alt="" class="slick-slide" data-slick-index="1" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide11"><img src="{{url('/')}}/front/images/3.jpg" alt="" class="slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide12"><img src="{{url('/')}}/front/images/4.jpg" alt="" class="slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide13"></div></div>



                </div>
            </div>
            <div class="center">
                <h3 class="m_t_0">Samsung UA32M5000AS - 32” TV - HD LED</h3>
                <div class="size options">
                    <p>Size</p>
                    <a href="#">32”</a>
                    <a href="#">43”</a>
                    <a href="#">58”</a>
                </div>
                <div class="color options">
                    <p>Color</p>
                    <a href="#">32”</a>
                    <a href="#">43”</a>
                    <a href="#">58”</a>
                </div>
                <div class="other_details">
                    <div class="price">
                        <p class="after">EGP 3,290</p>
                        <p class="before"><span class="price_value">EGP 4,330</span> <span class="discount_value">-12%</span></p>
                    </div>
                    <div class="actions">
                        <button type="button" class="add">Add To Cart</button>
                        <button type="button" class="save"><i class="mIcon"></i> Save for later</button>
                    </div>
                    <!-- <div class="key_features">
                        <h3>Key Features</h3>
                        <ul>
                            <li>- Screen Size: 32 inch</li>
                            <li>- Panel Backlight: LED</li>
                            <li>- Resolution: HD 1366 x 768</li>
                            <li>- Connectivity: 2 x HDMI, 1 x USB</li>
                            <li>- Watch movies from your USB</li>
                        </ul>
                    </div> -->
                </div>
                <!-- <div class="share">
                    <span>Share</span>
                    <div class="social">
                        <a href="#"><img src="{{url('/')}}/front/images/facebook_share.svg" alt=""></a>
                        <a href="#"><img src="{{url('/')}}/front/images/twitter_share.svg" alt=""></a>
                        <a href="#"><img src="{{url('/')}}/front/images/google_share.svg" alt=""></a>
                    </div>
                </div> -->
            </div>
            <div class="right">
                <div class="pay_features">
                    <div>
                        <img src="{{url('/')}}/front/images/notes.svg" alt="">
                        <p>Pay cash on delivery</p>
                    </div>
                    <div>
                        <img src="{{url('/')}}/front/images/shopping-store.svg" alt="">
                        <p>Pay cedit card online</p>
                    </div>
                    <div>
                        <img src="{{url('/')}}/front/images/refresh.svg" alt="">
                        <p>14 days free return</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="inner_section white">
            <h4 class="m_t_0">Frequently bought together</h4>
            <div class="combined">
                <div class="products">
                    <img src="{{url('/')}}/front/images/product.jpg" alt="">
                    <span>+</span>
                    <img src="{{url('/')}}/front/images/product_2.jpg" alt="">
                </div>
                <div class="price">
                    <p>Price for both: <b>EGP 3,840</b></p>
                    <button type="button">ADD ALL TO CART</button>
                </div>
            </div>
            <div class="choices">
                <div class="checkbox">
                    <input type="checkbox" id="item">
                    <label for="item"><b class="red">This Item: </b>Samsung UA32M5000AS - 32” TV - HD LED - EGP 3,290</label>
                </div>
                <div class="checkbox">
                    <input type="checkbox" id="wall_mount">
                    <label for="wall_mount">Wall mount - EGP 550</label>
                </div>
            </div>
        </section>
        {{--
        <section class="inner_section no_padding">
            <h4>Most Viewed in this Category</h4>
            <div class="products carousel flickity-enabled is-draggable" tabindex="0">

                <div class="flickity-viewport" style="height: 338px; touch-action: pan-y;"><div class="flickity-slider" style="left: 0px; transform: translateX(-0.36%);"><div aria-selected="true" class="is-selected" style="position: absolute; left: 0%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 14.6%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 29.2%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 43.8%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 58.39%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 72.99%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 87.59%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 102.19%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 116.79%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 131.39%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div></div></div><button class="flickity-button flickity-prev-next-button previous" type="button" disabled="" aria-label="Previous"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow"></path></svg></button><button class="flickity-button flickity-prev-next-button next" type="button" aria-label="Next"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg></button></div>
        </section>
        --}}
        <section class="inner_section white">
            <h4 class="m_t_0">Specification</h4>
            <dl>
                <dt>Brand</dt>
                <dd>Samsung</dd>
                <dt>Size</dt>
                <dd>34”</dd>
                <dt>Color</dt>
                <dd>Space Grey</dd>
                <dt>Resolution</dt>
                <dd>4K - 1920 px, 1280 px</dd>
            </dl>
            <a href="#" class="red_link small">Read More</a>
        </section>
        <section class="inner_section white">
            <h4 class="m_t_0">Description</h4>
            <div class="show_more_content">
                <p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nunc justo, convallis et sagittis sed, finibus ut erat. Ut et lacus sapien. Nunc interdum placerat arcu vel venenatis. Donec fermentum vitae velit ut hendrerit. Nunc sit amet
                    metus sed magna cursus bibendum. Sed laoreet elementum mauris. Etiam non semper dolor.
                </p>
                <p class="">Aliquam in augue sed massa pulvinar sodales. Etiam et scelerisque ligula. Aenean ultrices pharetra elementum. In vitae ultrices neque, et pellentesque urna. In euismod diam quam, sit amet hendrerit urna tristique a. Proin vel est et purus
                    semper vulputate eu nec turpis. Aenean volutpat at metus a sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum porttitor tortor eget cursus auctor. Nam a tellus at nibh blandit convallis id vel nisi.
                    Nulla lacinia ac purus eu venenatis. Cras consectetur urna at dolor pellentesque venenatis. Pellentesque eget dolor id leo congue sollicitudin. Sed non arcu ut arcu lobortis eleifend vitae maximus dolor. Ut ac massa in sem efficitur
                    volutpat. Fusce tincidunt tempus aliquam. Sed finibus nisl diam, vel imperdiet arcu bibendum id. Suspendisse interdum rutrum mauris, a convallis est porttitor et. Mauris tincidunt tellus sed orci consectetur iaculis. Donec iaculis
                    eget diam vel bibendum. Praesent fermentum ex nunc. Pellentesque efficitur sit amet tellus id congue. Vestibulum mattis metus libero, at porttitor diam imperdiet sit amet. Nunc laoreet justo id mauris tristique mollis.</p>
                <a href="#" class="colored show_more">Show More</a>
            </div>
        </section>
    {{--
        <section class="inner_section no_padding">
            <h4>More TVs</h4>
            <div class="products carousel flickity-enabled is-draggable" tabindex="0">

                <div class="flickity-viewport" style="height: 338px; touch-action: pan-y;"><div class="flickity-slider" style="left: 0px; transform: translateX(-0.36%);"><div aria-selected="true" class="is-selected" style="position: absolute; left: 0%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 14.6%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 29.2%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 43.8%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 58.39%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 72.99%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 87.59%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 102.19%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 116.79%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div><div aria-selected="false" style="position: absolute; left: 131.39%;">
                            <a href="#">
                                <div class="badge"><span>33</span></div>
                                <figure>
                                    <img src="{{url('/')}}/front/images/tv.jpg" alt="">
                                </figure>
                                <h3>Samsung UHD LED - 40” Class H5003 LED TV</h3>
                                <div class="price">
                                    <p class="before"><span>32.23 EGP</span> <span>10% OFF</span></p>
                                    <p class="after"><span>19.99</span> EGP <img src="{{url('/')}}/front/images/add-to-cart.svg" alt="Add To Cart"></p>
                                </div>
                            </a>
                        </div></div></div><button class="flickity-button flickity-prev-next-button previous" type="button" disabled="" aria-label="Previous"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow"></path></svg></button><button class="flickity-button flickity-prev-next-button next" type="button" aria-label="Next"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg></button></div>
        </section>
    --}}
    </div>
</div>
@stop
