




@foreach($ordersmore as $order)
    <section class="inner_section white">
        <div class="quick_info">
            <dl>
                <dt>Order placed on:</dt>
                <dd><strong>{{\Modules\Orders\Models\Order::CheckStatusDate($order->id)}}</strong></dd>
                <dt>Order ID:</dt>
                <dd>#{{$order->id}}</dd>
                <dt><a href="{{url('orders/'.$order->id)}}" class="colored">Order Details</a></dt>
            </dl>
            <dl>
                <dt>Recipient:</dt>
                <dd>{{auth()->user()->first_name .' '.auth()->user()->last_name }}</dd>
                <dt>Payment Method:</dt>
                <dd>Credit Card</dd>
                <dt>Total:</dt>
                <dd>{{\Modules\Orders\Models\Order::productAfterDiscountTotal($order->id)}} <span class="small">EGP</span></dd>
            </dl>
        </div>
        {{--<div class="line_separator m_t_20 m_b_20"></div>--}}
        {{--<p>Shipment 1 of 1</p>--}}
        {{--<div class="shipment_items">--}}
        {{--<div>--}}
        {{--<div class="image">--}}
        {{--<img src="images/box_1.jpg" alt="">--}}
        {{--</div>--}}
        {{--<div class="details">--}}
        {{--<div class="title">--}}
        {{--<h6><a href="#" class="colored">Team Force by Addidas for Men</a></h6>--}}
        {{--</div>--}}
        {{--<p>Shipped By: <a href="#" class="colored">Arafa</a></p>--}}
        {{--<p class="m_t_10">Return window closed on <strong>22 March 2018, item cannot be returned</strong></p>--}}
        {{--<!-- <p>Condition: <span>New</span></p> -->--}}
        {{--<!-- <div class="rating">--}}
        {{--<p>Rate this product</p>--}}
        {{--<div class="stars">--}}
        {{--<span><i class="mIcon"></i></span>--}}
        {{--<span><i class="mIcon"></i></span>--}}
        {{--<span><i class="mIcon"></i></span>--}}
        {{--<span><i class="mIcon"></i></span>--}}
        {{--<span><i class="mIcon"></i></span>--}}
        {{--</div>--}}
        {{--</div> -->--}}
        {{--</div>--}}
        {{--<!-- <div class="item_feedback">--}}
        {{--<p>Seller: <a href="#" class="colored">Arafa</a></p>--}}
        {{--<a href="#" class="btn btn-basic btn-block go_success">Leave a seller feedback</a>--}}

        {{--</div> -->--}}
        {{--</div>--}}
        {{--</div>--}}

    </section>
@endforeach
<button type="button" class="btn btn-basic btn-block more">Show More</button>







