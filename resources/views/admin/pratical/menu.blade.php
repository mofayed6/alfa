<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            <img src="{{ asset('/assets/images/user.png') }}" class="rounded-circle user-photo" alt="User Profile Picture">
            <div class="dropdown">
                <span>Welcome,</span>
                <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>{{Auth('admin')->user()->name}}</strong></a>
                <ul class="dropdown-menu dropdown-menu-right account">
                    {{--<li><a href="page-profile2.html"><i class="icon-user"></i>My Profile</a></li>--}}
                    {{--<li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Messages</a></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>--}}
                    {{--<li class="divider"></li>--}}

                    <li>  <a  href="{{ route('doLogout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"> <i class="icon-power"></i>
                            {{ __('Logout') }}
                        </a></li>

                        <form id="logout-form" action="{{ route('doLogout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                </ul>
            </div>
            <hr>
            {{--<ul class="row list-unstyled">--}}
                {{--<li class="col-4">--}}
                    {{--<small>Sales</small>--}}
                    {{--<h6>456</h6>--}}
                {{--</li>--}}
                {{--<li class="col-4">--}}
                    {{--<small>Order</small>--}}
                    {{--<h6>1350</h6>--}}
                {{--</li>--}}
                {{--<li class="col-4">--}}
                    {{--<small>Revenue</small>--}}
                    {{--<h6>$2.13B</h6>--}}
                {{--</li>--}}
            {{--</ul>--}}
        </div>
        <!-- Nav tabs -->
        {{--<ul class="nav nav-tabs">--}}
            {{--<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>--}}
            {{--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Chat"><i class="icon-book-open"></i></a></li>--}}
            {{--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#setting"><i class="icon-settings"></i></a></li>--}}
            {{--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#question"><i class="icon-question"></i></a></li>--}}
        {{--</ul>--}}

        <!-- Tab panes -->
        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane active" id="menu">
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul id="main-menu" class="metismenu">
                        <li class="{{request()->is('admin')?'active':''}}">
                            <a href="#Dashboard" class="has-arrow"><i class="icon-home"></i> <span>Dashboard</span></a>
                            <ul>
                                <li class="active"><a href="index.html">Analytical</a></li>
                            </ul>
                        </li>
                        @if(adminHasPermissions('admins') || adminHasPermissions('rules')  )
                        <li class="{{(request()->is('admin/admins*') || request()->is('admin/rules*') )?'active':''}}">
                            <a href="{{url('admin/admins')}}" class="has-arrow"><i class="icon-grid"></i> <span>Admins</span></a>
                            <ul>
                                @if(adminHasPermissions('rules') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/rules*')?'active':''}}"><a href="{{url('admin/rules')}}">Admins Rules</a></li>
                                @endif
                                @if(adminHasPermissions('admins') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/admins*')?'active':''}}"><a href="{{url('admin/admins')}}">Admins</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif
                        @if(adminHasPermissions('locations') || auth('admin')->user()->rule_id == 1)
                        <li class="{{(request()->is('admin/locations*'))?'active':''}}">
                            <a href="{{url('admin/locations')}}"><i class="icon-grid"></i> <span>Locations</span></a>
                        </li>
                        @endif


                        @if(adminHasPermissions('deliverylocations') || auth('admin')->user()->rule_id == 1)
                            <li class="{{(request()->is('admin/deliverylocations*'))?'active':''}}">
                                <a href="{{url('admin/deliverylocations')}}"><i class="icon-grid"></i> <span>Delivery Locations</span></a>
                            </li>
                        @endif

                        @if(adminHasPermissions('branches') || auth('admin')->user()->rule_id == 1)
                        <li class="{{(request()->is('admin/branches*'))?'active':''}}">
                            <a href="{{url('admin/branches')}}"><i class="icon-grid"></i> <span>Branches</span></a>
                        </li>
                        @endif

                        @if(adminHasPermissions('categories') || adminHasPermissions('attributes'))
                        <li class="{{(request()->is('admin/categories*') || request()->is('admin/attributes*'))?'active':''}}">
                            <a href="{{url('admin/categories')}}" class="has-arrow"><i class="icon-grid"></i> <span>Categories</span></a>
                            <ul>
                                @if(adminHasPermissions('categories') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/categories*')?'active':''}}"><a href="{{url('admin/categories')}}">Categories</a></li>
                                @endif
                                @if(adminHasPermissions('attributes') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/attributes*')?'active':''}}"><a class="{{request()->is('admin/attributes*')?'active':''}}" href="{{url('admin/attributes')}}">Attributes</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif

                        @if(adminHasPermissions('classifications') || adminHasPermissions('units') || adminHasPermissions('identifiers') || adminHasPermissions('products'))
                        <li class="{{(
                        request()->is('admin/classifications*') ||
                        request()->is('admin/units*') ||
                        request()->is('admin/identifiers*') ||
                        request()->is('admin/products*')
                        )?'active':''}}">
                            <a href="{{url('admin/products')}}" class="has-arrow"><i class="icon-grid"></i> <span>Products</span></a>
                            <ul>
                                @if(adminHasPermissions('classifications') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/classifications*')?'active':''}}"><a href="{{url('admin/classifications')}}">Classifications</a></li>
                                @endif
                                @if(adminHasPermissions('units') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/units*')?'active':''}}"><a href="{{url('admin/units')}}">Units</a></li>
                                @endif
                                @if(adminHasPermissions('identifiers') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/identifiers*')?'active':''}}"><a href="{{url('admin/identifiers')}}">Identifiers</a></li>
                                @endif
                                @if(adminHasPermissions('products') || auth('admin')->user()->rule_id == 1)
                                    <li class="{{request()->is('admin/products*')?'active':''}}"><a href="{{url('admin/products')}}">Products</a></li>
                                @endif
                            </ul>
                        </li>

                            @if(adminHasPermissions('orders') || auth('admin')->user()->rule_id == 1)
                                <li class="{{(request()->is('admin/orders*'))?'active':''}}">
                                    <a href="{{url('admin/orders')}}"><i class="icon-grid"></i> <span>Orders</span></a>
                                </li>
                            @endif

                        @if(adminHasPermissions('coupons') || auth('admin')->user()->rule_id == 1)
                            <li class="{{(request()->is('admin/coupons*'))?'active':''}}">
                                <a href="{{url('admin/coupons')}}"><i class="icon-grid"></i> <span>Coupons</span></a>
                            </li>
                        @endif
                        @endif
                        @if(adminHasPermissions('user') || auth('admin')->user()->rule_id == 1)
                            <li class="{{(request()->is('admin/user*'))?'active':''}}">
                                <a href="{{url('admin/user')}}"><i class="icon-grid"></i> <span>User</span></a>
                            </li>
                        @endif

                    </ul>
                </nav>
            </div>
            <div class="tab-pane p-l-15 p-r-15" id="Chat">
                <form>
                    <div class="input-group m-b-20">
                        <div class="input-group-prepend">
                            <span class="input-group-text" ><i class="icon-magnifier"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Search...">
                    </div>
                </form>
                <ul class="right_chat list-unstyled">
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="{{ asset('/assets/images/xs/avatar4.jpg') }}" alt="">
                                <div class="media-body">
                                    <span class="name">Chris Fox</span>
                                    <span class="message">Designer, Blogger</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="{{ asset('/assets/images/xs/avatar5.jpg') }}" alt="">
                                <div class="media-body">
                                    <span class="name">Joge Lucky</span>
                                    <span class="message">Java Developer</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="{{ asset('/assets/images/xs/avatar2.jpg') }}" alt="">
                                <div class="media-body">
                                    <span class="name">Isabella</span>
                                    <span class="message">CEO, Thememakker</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="{{ asset('/assets/images/xs/avatar1.jpg') }}" alt="">
                                <div class="media-body">
                                    <span class="name">Folisise Chosielie</span>
                                    <span class="message">Art director, Movie Cut</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="online">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <img class="media-object " src="{{ asset('/assets/images/xs/avatar3.jpg') }}" alt="">
                                <div class="media-body">
                                    <span class="name">Alexander</span>
                                    <span class="message">Writter, Mag Editor</span>
                                    <span class="badge badge-outline status"></span>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tab-pane p-l-15 p-r-15" id="setting">
                <h6>Choose Skin</h6>
                <ul class="choose-skin list-unstyled">
                    <li data-theme="purple">
                        <div class="purple"></div>
                        <span>Purple</span>
                    </li>
                    <li data-theme="blue">
                        <div class="blue"></div>
                        <span>Blue</span>
                    </li>
                    <li data-theme="cyan" class="active">
                        <div class="cyan"></div>
                        <span>Cyan</span>
                    </li>
                    <li data-theme="green">
                        <div class="green"></div>
                        <span>Green</span>
                    </li>
                    <li data-theme="orange">
                        <div class="orange"></div>
                        <span>Orange</span>
                    </li>
                    <li data-theme="blush">
                        <div class="blush"></div>
                        <span>Blush</span>
                    </li>
                </ul>
                <hr>
                <h6>General Settings</h6>
                <ul class="setting-list list-unstyled">
                    <li>
                        <label class="fancy-checkbox">
                            <input type="checkbox" name="checkbox">
                            <span>Report Panel Usag</span>
                        </label>
                    </li>
                    <li>
                        <label class="fancy-checkbox">
                            <input type="checkbox" name="checkbox" checked>
                            <span>Email Redirect</span>
                        </label>
                    </li>
                    <li>
                        <label class="fancy-checkbox">
                            <input type="checkbox" name="checkbox" checked>
                            <span>Notifications</span>
                        </label>
                    </li>
                    <li>
                        <label class="fancy-checkbox">
                            <input type="checkbox" name="checkbox">
                            <span>Auto Updates</span>
                        </label>
                    </li>
                    <li>
                        <label class="fancy-checkbox">
                            <input type="checkbox" name="checkbox">
                            <span>Offline</span>
                        </label>
                    </li>
                    <li>
                        <label class="fancy-checkbox">
                            <input type="checkbox" name="checkbox">
                            <span>Location Permission</span>
                        </label>
                    </li>
                </ul>
            </div>
            <div class="tab-pane p-l-15 p-r-15" id="question">
                <form>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" ><i class="icon-magnifier"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Search...">
                    </div>
                </form>
                <ul class="list-unstyled question">
                    <li class="menu-heading">HOW-TO</li>
                    <li><a href="javascript:void(0);">How to Create Campaign</a></li>
                    <li><a href="javascript:void(0);">Boost Your Sales</a></li>
                    <li><a href="javascript:void(0);">Website Analytics</a></li>
                    <li class="menu-heading">ACCOUNT</li>
                    <li><a href="javascript:void(0);">Cearet New Account</a></li>
                    <li><a href="javascript:void(0);">Change Password?</a></li>
                    <li><a href="javascript:void(0);">Privacy &amp; Policy</a></li>
                    <li class="menu-heading">BILLING</li>
                    <li><a href="javascript:void(0);">Payment info</a></li>
                    <li><a href="javascript:void(0);">Auto-Renewal</a></li>
                    <li class="menu-button m-t-30">
                        <a href="javascript:void(0);" class="btn btn-primary"><i class="icon-question"></i> Need Help?</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
