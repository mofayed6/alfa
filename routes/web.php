<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'guest:web'],function (){
    Route::get('login', 'Auth\AuthController@login');
    Route::post('login', 'Auth\AuthController@doLogin');
    Route::get('register', 'Auth\AuthController@register');
    Route::post('register', 'Auth\AuthController@doRegister');
});
Route::group(['middleware'=>'auth:web'],function (){
    Route::get('logout', 'Auth\AuthController@logout');
});

Route::group(['prefix'=>'email/verify'],function (){
    Route::get('{email}', 'Auth\VerifyController@verifyPage');
    Route::post('{email}', 'Auth\VerifyController@manualVerify');

    Route::post('{email}/resend', 'Auth\VerifyController@resendVerifyToken');
    Route::get('{email}/{token}', 'Auth\VerifyController@verify');
});


Route::get('/', 'HomeController@index');







