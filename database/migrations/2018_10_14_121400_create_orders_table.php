<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('coupon_id');
            $table->unsignedInteger('user_id');
            $table->Integer('tex_percentage');
            $table->timestamps();
        });
        //set integers length
        Schema::table('orders', function (Blueprint $table) {
            DB::statement('ALTER TABLE '.$table->getTable().' MODIFY COLUMN branch_id INT(4)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN coupon_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN tex_percentage INT(2)');
            //foreign key constrains
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
