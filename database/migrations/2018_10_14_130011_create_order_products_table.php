<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->decimal('price');
            $table->decimal('discount_price')->nullable();
            $table->integer('quantity')->nullable();
            $table->timestamp('discount_from');
            $table->timestamp('discount_to')->useCurrent();
            $table->timestamps();
        });
        //set integers length
        Schema::table('order_products', function (Blueprint $table) {
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN quantity INT(5)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
