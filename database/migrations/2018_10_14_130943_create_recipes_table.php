<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video',255);
            $table->unsignedInteger('species_id');
            $table->timestamps();
        });
        Schema::table('recipes', function (Blueprint $table) {
            //set integers length
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN species_id INT(3)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
