<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 1000; $i++) {
            App\Admin::create([
                'id'=>1,
                'name' => $faker->name,
                'email' => $faker->email,
                'password'=>bcrypt('123456')
            ]);
        }
    }
}
