<?php

namespace Modules\Classifications\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Classifications\Models\Classification;

class ClassificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('classifications::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('classifications::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'ar.title'=>'required',
            'en.title'=>'required',
        ]);
        $classification = new Classification();
        $classification->save();
        $classification->translateAttributes($request->all());
        return redirect('admin/classifications')->with(['success'=>'Classification Created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('classifications::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Classification $classification)
    {
        return view('classifications::edit',compact('classification'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Classification $classification,Request $request)
    {
        $this->validate($request,[
            'ar.title'=>'required',
            'en.title'=>'required',
        ]);
        $classification->save();
        $classification->deleteTranslations();
        $classification->translateAttributes($request->all());
        return redirect('admin/classifications')->with(['success'=>'Classification Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Classification $classification)
    {
        $classification->delete();
        $classification->deleteTranslations();
        return redirect('admin/classifications')->with(['success'=>'Classification Deleted Successfully']);

    }

    public function datatable()
    {
        $classification = Classification::all();
        return \DataTables::of($classification)
            ->editColumn('title',function ($model){
                return $model->title;
            })->editColumn('options',function ($model){
                return "<a href='".url('admin/classifications/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/classifications/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);

    }

}
