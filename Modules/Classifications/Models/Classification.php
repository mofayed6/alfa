<?php

namespace Modules\Classifications\Models;

use App\Http\Traits\TranslatableTrait;
use App\Http\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Model;

class Classification extends Model
{
    use TranslatableTrait;
    protected $table        = 'classifications';
    protected $translatable  = ['title'];
}
