@extends('admin.layouts.master')
@section('page-title','Create Classification')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{url("admin/classifications")}}">Classifications</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Create new Products Classification</h2>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::model($classification,['action'=>['\Modules\Classifications\Http\Controllers\ClassificationsController@update',$classification->id],'method'=>'PATCH'])}}
                @include('classifications::form')
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('scripts')

@stop
