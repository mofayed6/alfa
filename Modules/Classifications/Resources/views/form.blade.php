<ul class="nav nav-tabs">
    @foreach(locales() as $k=>$local)
        <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach(locales() as $k=>$local)
        <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
            <div class="form-group">
                <label>slug {{ $local->code}} </label>
                {{Form::text($local->code.'[title]',null,['class'=>'form-control'])}}
            </div>
        </div>
    @endforeach
    <button type="submit" class="btn btn-success">Save</button>
</div>
