<?php

namespace Modules\Coupons\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Categories\Models\Category;
use Modules\Products\Models\Product;

class Coupon extends Model
{
    protected $fillable = [];

    public function products()
    {
        return $this->belongsToMany(Product::class,'coupon_products','coupon_id','product_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'coupon_categories','coupon_id','category_id');
    }
}
