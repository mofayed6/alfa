<?php

namespace Modules\Coupons\Rules;


use Illuminate\Validation\Concerns\ValidatesAttributes;

class BetweenIf
{
    use ValidatesAttributes;


    public function message()
    {
        return 'The :attribute can be only from :min to :max when the :other is :value';
    }


    /**
     * Validate that the value of attribute is between min and max when another attribute is equal to a given value
     *
     * @param  string  $attribute
     * @param  mixed   $value
     * @param  mixed   $parameters
     * @param  object   $validator
     * @return bool
    */
    public function validateBetweenIf($attribute, $value, $parameters,$validator) {
        $this->requireParameterCount(4, $parameters, 'between_if');
        if ($parameters[3] == $validator->getData()[$parameters[2]]) {
            return $value >= $parameters[0] && $value <= $parameters[1];
        }
        return true;
    }


    /**
    * Replace all place-holders for the between_if rule.
    *
    * @param  string  $message
    * @param  string  $attribute
    * @param  string  $rule
    * @param  array   $parameters
    * @return string
    */
    public function replaceBetweenIf($message, $attribute, $rule, $parameters) {
        $otherValue = $parameters[3];
        return str_replace(array(':attribute',':min', ':max',':other', ':value'), array_merge([$attribute],$parameters), $this->message());
    }
}
