<div class="form-group">
    <label>Code</label>
    {{Form::text('code',$code,['class'=>'form-control','readonly'])}}
</div>

<div class="form-group">
    <label> Discount  </label>
    <div class="input-group">
        <span class="input-group-addon">
            {{Form::select('amount_type',['fixed'=>'fixed','percentage'=>'percentage %'],null,['class'=>'form-control'])}}
        </span>
            {{Form::number('amount',null,['class'=>'form-control'])}}
    </div><!-- /input-group -->
</div>



<div class="form-group">
    <label>Apply To</label>
    {{Form::select('type',['All Cart Items','All Cart Items with minimum amount','Specific Products','Specific Categories'],null,['class'=>'form-control',])}}
</div>


<div id="type_1" class="typeOption" style="display: none">
    <div class="form-group">
        <label>Minimum Amount ( EGP )</label>
        {{Form::number('minimum_amount',null,['class'=>'form-control'])}}
    </div>
</div>

<div id="type_2" class="typeOption" style="display: none">
    <div class="form-group">
        <label>Select Products</label>
        {{Form::select('products[]',[],null,['class'=>'form-control select2','id'=>'products','multiple'])}}
    </div>
</div>

<div id="type_3" class="typeOption" style="display: none">
    <div class="form-group">
        <label>Select Categories</label>
        {{Form::select('categories[]',$categories,null,['class'=>'form-control categories_select2','multiple'])}}
    </div>
</div>

<div class="form-group">
    <label>Coupon Date</label>
    {{Form::text('date',null,['class'=>'form-control','readonly'])}}
</div>

<div class="form-group">
    <button type="submit" class="btn btn-success"> Save </button>
</div>


@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/css/select2.min.css') }}">

@endsection
@section('scripts')
    <script src="{{ asset('assets/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>
    <script>
            $(document).ready(function () {
                typeVal = $('select[name="type"]').val();
                    $('.typeOption').hide();
                    if(typeVal == 2){
                        $('#type_2').show();
                    }else if(typeVal == 3){
                        $('#type_3').show();
                    }else if(typeVal == 1){
                        $('#type_1').show();
                    }
            });

            $('select[name="type"]').on('change',function (type) {
                $('.typeOption').hide();
                if($(this).val() == 2){
                    $('#type_2').show();
                }else if($(this).val() == 3){
                    $('#type_3').show();
                }else if($(this).val() == 1){
                    $('#type_1').show();
                }
            });

            $('input[name="date"]').daterangepicker({
                minDate: new Date(),
                timePicker: true,
                locale: {
                    format: 'Y-M-DD HH:mm:ss'
                }
            });


            $('.select2').select2({
                width: "100%",
                ajax: {
                    url: 'http://alfa-market.test/admin/coupons/search_products/',
                    dataType: 'json',
                    processResults: function (ArrayData) {
                        return {
                            results: $.map(ArrayData, function (obj) {
                                obj.text = obj.text || obj.en.name + " - " + obj.ar.name; // replace name with the property used for the text
                                return obj;
                            }),
                        };
                    },
                },
            });



            $('.categories_select2').select2({
                width:"100%",
            });

    </script>
@endsection
