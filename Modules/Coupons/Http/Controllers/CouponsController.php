<?php

namespace Modules\Coupons\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Categories\Models\Category;
use Modules\Coupons\Models\Coupon;
use Modules\Products\Models\Product;

class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('coupons::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $code       = str_random(6);
        $categories = Category::getChildrenFlat();
        return view('coupons::create',compact('code','categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'code'          => 'required',
            'type'          => 'required',
            'amount'        => 'required|integer|between_if:0,100,amount_type,percentage',
            'minimum_amount'=> 'required_if:type,1|integer|min:1',
            'products'      => 'required_if:type,2',
            'categories'    => 'required_if:type,3',
            'date'          => 'required',
        ],[
            'minimum_amount.required_if'=>'The Minimum Amount field is required when Apply To Minimum Amount.',
            'products.required_if'=>'The products field is required when Apply To Specific Products.',
            'categories.required_if'=>'The Minimum Amount field is required when Apply To Specific Categories.',
        ]);

        $date = explode(' - ',$request->get('date'));

        $coupon = new Coupon();
        $coupon->code       = $request->get('code');
        $coupon->type       = $request->get('type');
        $coupon->amount_type= $request->get('amount_type');
        $coupon->amount     = $request->get('amount');
        $coupon->from       = $date[0];
        $coupon->to         = $date[1];

        if($request->get('type') == 1)
            $coupon->minimum_amount = $request->get('minimum_amount');
        $coupon->save();

        if($request->get('type') == 2)
            $coupon->products()->sync($request->get('products'));

        if($request->get('type') == 3)
            $coupon->categories()->sync($request->get('categories'));

        return redirect('admin/coupons')->with(['success'=>'coupon created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('coupons::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Coupon $coupon)
    {
        $code = $coupon->code;
        $categories = Category::getChildrenFlat();
        $coupon->date = implode(' - ',[$coupon->from,$coupon->to]);
        return view('coupons::edit',compact('coupon','code','categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Coupon $coupon,Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'type' => 'required',
            'amount' => 'required|integer|between_if:0,100,amount_type,percentage',
            'minimum_amount' => 'required_if:type,1|integer',
            'products' => 'required_if:type,2',
            'categories' => 'required_if:type,3',
            'date' => 'required',
        ], [
            'minimum_amount.required_if' => 'The Minimum Amount field is required when Apply To Minimum Amount.',
            'products.required_if' => 'The products field is required when Apply To Specific Products.',
            'categories.required_if' => 'The Minimum Amount field is required when Apply To Specific Categories.',
        ]);

        $date = explode(' - ', $request->get('date'));

        $coupon->code = $request->get('code');
        $coupon->type = $request->get('type');
        $coupon->amount_type = $request->get('amount_type');
        $coupon->amount = $request->get('amount');
        $coupon->from = $date[0];
        $coupon->to = $date[1];

        if ($request->get('type') == 1) {
            $coupon->minimum_amount = $request->get('minimum_amount');
            $coupon->products()->detach();
            $coupon->categories()->detach();
        }
        $coupon->save();

        if ($request->get('type') == 2) {
            $coupon->products()->sync($request->get('products'));
            $coupon->categories()->detach();
        }

        if ($request->get('type') == 3){
            $coupon->categories()->sync($request->get('categories'));
            $coupon->products()->detach();
        }
        return redirect('admin/coupons')->with(['success'=>'coupon created successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        return redirect('admin/coupons')->with(['success'=>'coupon deleted successfully']);
    }

    public function datatable()
    {
        $coupons = Coupon::query();
        return \DataTables::eloquent($coupons)
            ->addColumn('options',function ($model){
                return "<a href='".url('admin/coupons/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/coupons/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make();
    }

    public function searchProducts(Request $request)
    {
        $products = Product::whereHas('translations',function($query) use($request){
            return $query->where('value','like','%'.$request->q.'%');
        })->get();

        return $products;
    }
}
