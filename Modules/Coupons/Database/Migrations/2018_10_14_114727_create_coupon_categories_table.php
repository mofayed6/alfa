<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('coupon_id');
            $table->timestamps();
        });
        //set integers length
        Schema::table('coupon_categories', function (Blueprint $table) {
            DB::statement('ALTER TABLE '.$table->getTable().' MODIFY COLUMN id INT(5) AUTO_INCREMENT');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN category_id INT(3)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN coupon_id INT(7)');
            //foreign key constrains
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_categories');
    }
}
