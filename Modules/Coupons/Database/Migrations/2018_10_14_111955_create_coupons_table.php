<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',6);
            $table->boolean('type')->comment('0=>all cart | 1 => all cart with minimum | 2=> Spe. products | 3 => spe. categories');
            $table->integer('minimum_amount');
            $table->enum('amount_type',['fixed','percentage'])->default('fixed');
            $table->integer('amount');
            $table->timestamp('from');
            $table->timestamp('to');
            $table->timestamps();
        });
        //set integers length
        Schema::table('coupons', function (Blueprint $table) {
            DB::statement('ALTER TABLE '.$table->getTable().' MODIFY COLUMN id INT(7) AUTO_INCREMENT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
