<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','CartController@index');
Route::post('/','CartController@store');
Route::patch('{item}','CartController@update');
Route::delete('{item}','CartController@destroy');

