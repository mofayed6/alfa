<?php

namespace Modules\Languages\Models;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $table='translations';

    public function getValueAttribute($val)
    {
        $attribute = $this['attribute'];
        $model = new $this->model;
        $translatable = $model->getTranslatable();
        $casts = $model->getCasts();
        if(in_array($attribute,array_keys($casts)) && in_array($attribute,$translatable)){
            switch ($casts[$attribute]){
                case 'json':
                    return json_decode($val);
                    break;
            }
        }
        return $val;

    }

}
