<?php

namespace Modules\Identifiers\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Identifiers\Models\Identifier;

class IdentifiersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('identifiers::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('identifiers::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'identifier'=>'required',
        ]);
        Identifier::create($request->all());
        return redirect('admin/identifiers')->with(['success'=>'Identifier Created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('identifiers::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Identifier $identifier)
    {
        return view('identifiers::edit',compact('identifier'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,Identifier $identifier)
    {
        $this->validate($request,[
            'identifier'=>'required',
        ]);
        $identifier->update($request->all());
        return redirect('admin/identifiers')->with(['success'=>'Identifiers updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Identifier $identifier)
    {
        $identifier->delete();
        return back()->with(['success'=>'Identifiers deleted successfully']);
    }

    public function datatable()
    {
        $identifier = Identifier::query();
        return \DataTables::eloquent($identifier)
            ->addColumn('options',function ($model){
                return "<a href='".url('admin/identifiers/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/identifiers/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";
            })
            ->make();
    }
}
