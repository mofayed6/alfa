@extends('admin.layouts.master')
@section('page-title','Identifiers')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Identifiers list</h2>
                </div>
                <div class="col-md-6">
                    <a href="{{url('admin/identifiers/create/')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Identifier</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/identifiers/datatable') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'identifier', name: 'identifier' },
                    { data: 'options', name: 'options' },
                ],
            });
        });
    </script>
@stop
