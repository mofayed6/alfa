@extends('admin.layouts.master')
@section('page-title',' Create Identifiers')
@section('breadcrumb')
    <li class="breadcrumb-item "><a href="{{url("admin/identifiers")}}"> Identifiers </a> </li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Create Identifiers</h2>
                </div>

            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::open(['action'=>'\Modules\Identifiers\Http\Controllers\IdentifiersController@store'])}}
                <div class="form-group">
                    <label>Identifier</label>
                    {{Form::text('identifier',null,['class'=>'form-control'])}}
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit"> Save </button>
                </div>
            {{Form::close()}}
        </div>
    </div>
@endsection

