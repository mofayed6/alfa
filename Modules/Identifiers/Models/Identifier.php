<?php

namespace Modules\Identifiers\Models;

use Illuminate\Database\Eloquent\Model;

class Identifier extends Model
{
    protected $fillable = [
        'identifier'
    ];
}
