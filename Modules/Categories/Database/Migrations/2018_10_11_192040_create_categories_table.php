<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->boolean('is_active')->default(1);
            $table->boolean('show_in_navbar')->default(1);
            $table->timestamps();
        });

        Schema::table('categories', function (Blueprint $table) {
            //set integers length
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN id INT(3) AUTO_INCREMENT');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN parent_id INT(3)');
            //foreign key constrains
            $table->foreign('parent_id')->references('id')->on($table->getTable())->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
