<?php

namespace Modules\Categories\Http\Controllers;

use Modules\Attributes\Models\Attribute;
use Modules\Languages\Models\Local;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use \App\Http\Controllers\Controller;
use Modules\Categories\Models\Category;
use Modules\Languages\Models\Translation;

class CategoriesController extends Controller
{
    protected $results = [];
    protected $i = 0;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($parent = null)
    {
        if($parent)
            $category = Category::findOrFail($parent);
        return view('categories::index',compact('parent'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request ,$parent = null)
    {


        if($parent)
            Category::findOrFail($request->parent);
        $locales = Local::all();
        $categories = Category::getChildrenFlat(null,3);
        if($parent = null) {
            $depth =  count(Category::getParentsFlat($parent));
            if($depth >= 4){
                abort(503);
            }
        }

        $parentCategories = Category::getParentsFlat($parent);
        $parentAttributes =  Attribute::getAttributesByCategoriesArray(array_keys($parentCategories));
        $parent = $request->parent;
        return view('categories::create', compact('locales', 'categories','parent','parentAttributes'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'en.slug'=>'required',
            'ar.name'=>'required',
            'ar.slug'=>'required',
        ]);
        $category = new Category();
        $category->parent_id        = $request->parent_id;
        $category->is_active        = $request->is_active ?? 0;
        $category->show_in_navbar   = $request->show_in_navbar ?? 0;
        $category->save();
        $category->translateAttributes($request->only('ar','en'));
        if($request->has('attributes')){
            $attrList = [];
            $attributes = json_decode($request->get('attributes'));
            if(is_array($attributes) && count($attributes) >0)
            foreach ($attributes as $attribute){
                $attrList[] =$attribute->id;
            }
            $category->attributes()->sync($attrList);
        }
         return redirect(url('admin/categories/'.$category->parent_id))->with(['success'=>'Category Created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('categories::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Category $category)
    {
        //$locales = Local::all();
        $categories = Category::getChildrenFlat(null,3);
        $attributes = $category->attributes;
        if(count($attributes)>0 ){
            $attributesList = [];
            foreach ($attributes as $k => $attribute){
                $attributesList[$k]['id'] = $attribute->id;
                $attributesList[$k]['name'] = $attribute->en['name'].' - '.$attribute->ar['name'];
            }
            $category->attributes_str = json_decode(json_encode($attributesList));
            $category->attributes     = json_encode($attributesList);
        }
        $parentCategories = Category::getParentsFlat($category->id);
        $parentAttributes =  Attribute::getAttributesByCategoriesArray(array_keys($parentCategories));
        $parent = $category->parent_id;
        return view('categories::edit',compact('category','categories','parent','parentAttributes'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Category $category,Request $request)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'en.slug'=>'required',
            'ar.name'=>'required',
            'ar.slug'=>'required',
        ]);
        $category->parent_id        = $request->parent_id;
        $category->is_active        = $request->is_active ?? 0;
        $category->show_in_navbar   = $request->show_in_navbar ?? 0;
        $category->save();
        $category->deleteTranslations();
        $category->translateAttributes($request->only('ar','en'));
        if($request->has('attributes')){
            $attrList = [];
            $attributes = json_decode($request->get('attributes'));
            if(is_array($attributes) && count($attributes) >0)
                foreach ($attributes as $attribute){
                    $attrList[] =$attribute->id;
                }
            $category->attributes()->sync($attrList);
        }
        return redirect(url('admin/categories/'.$category->parent_id))->with(['success'=>'Category Updated Successfully']);
    }


    /**
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
        $category->deleteTranslations();
        return redirect(url('admin/categories/'.$category->parent_id))->with(['success'=>'Category Deleted Successfully']);

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function datatable(Request $request)
    {
        $categories = Category::whereParentId($request->parent)->get();
        return \DataTables::of($categories)
            ->editColumn('name',function ($model){
                return "<a href='".url('admin/categories/'.$model->id)."'>".$model->name."</a>";
            })->editColumn('options',function ($model){
                return "<a href='".url('admin/categories/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/categories/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }

    public function slugify($string,$locale_code)
    {
        $slug = Category::makeSlug($string,$locale_code);
        return response()->json(['slug'=>$slug]);
    }
}
