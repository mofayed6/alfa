<ul class="nav nav-tabs">
    @foreach(locales() as $k=>$local)
        <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach(locales() as $k=>$local)
        <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
            <div class="form-group">
                <label>Name {{ $local->code}} </label>
                {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
            </div>
            <div class="form-group">
                <label>Description {{ $local->code}} </label>
                {{Form::textarea($local->code.'[description]',null,['class'=>'form-control','rows'=>3])}}
            </div>
            <div class="form-group">
                <label>slug {{ $local->code}} </label>
                {{Form::text($local->code.'[slug]',null,['class'=>'form-control','readonly'])}}
            </div>
        </div>
    @endforeach
    <div class="form-group">
        <label> Category </label>
        {{Form::select('parent_id',[null=>'New Main Category']+$categories,$parent ?? null,['class'=>'form-control'])}}
    </div>
    <div class="form-group">

        <label class="fancy-checkbox">
            {{Form::checkbox('is_active',1,null)}}
            <span>Active</span>
        </label>
    </div>
    <div class="form-group">
        <label class="fancy-checkbox">
            {{Form::checkbox('show_in_navbar',1,null)}}
            <span>Show In Navbar</span>
        </label>
    </div>

    <button type="submit" class="btn btn-success">Save</button>

</div>
