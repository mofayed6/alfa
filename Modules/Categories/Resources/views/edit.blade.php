@extends('admin.layouts.master')
@section('page-title','Edit '.$category->order)
@section('breadcrumb')
    <li class="breadcrumb-item {{(@$parent == null)?"active":""}}">@if(@$parent != null)<a href='{{url('admin/categories')}}'>Categories</a>@else Categories @endif</li>
    @if(@$parent != null)
        @foreach(\Modules\Categories\Models\Category::getParentsFlat($parent) as $id=>$name)
            <li class="breadcrumb-item {{($parent == $id)?'active':''}}"><a href="{{url('admin/categories/'.$id)}}">{{$name}}</a></li>
        @endforeach
    @endif
    <li class="breadcrumb-item active">Edit {{$category->name}}</li>
@endsection
@section('content')
    {{Form::model($category,['method'=>'PATCH','action'=>['\Modules\Categories\Http\Controllers\CategoriesController@update',$category->id]])}}
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="header">
                    <h2>Edit {{$category->name}}</h2>
                </div>
                <div class="body">
                    @include('admin.pratical.message')
                    @include('categories::form')
                    {{Form::hidden('attributes')}}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="display:none;" id="addWrapper">
                <div class="body">
                    {{Form::select('attr_string',[],null,['multiple','class'=>'form-control select2','id'=>'attributes'])}}
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>Category Attributes</h2>
                        </div>
                        <div class="col-md-4">
                            <a href="#add" id="add-attribute" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i></a>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div style="width: 100%;    padding-bottom: 15px;">
                        @foreach($parentAttributes as $attribute)
                        <li class="dd-item" data-slug="">
                            <div class="dd-handle">{{$attribute->en['name']}} - {{$attribute->ar['name']}}</div>
                        </li>
                        @endforeach
                    </div>
                    <div class="dd" id="nestable" style="width: 100%;    padding-bottom: 15px;">
                        <ol class="dd-list"></ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{Form::close()}}

@endsection
@section('styles')
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('assets/css/color_skins.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery/jquery-sortable.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/select2/css/select2.min.css') }}">
@endsection
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('assets/vendor/jquery/jquery-sortable.js') }}"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery/jquery-sortable.js') }}"></script>

    {{--    <script src="{{ asset('assets/vendor/select2sortable/select2.sortable.min.js') }}"></script>--}}
    <script>
        attributesList = [];
        jsonAttributes =[];
        $(document).ready(function () {
            getOptionsFromInput($('input[name="attributes"]').val());
            $('.dd').nestable();
            $('.select2').select2({
                width:"100%",
                ajax: {
                    url: '{{url('admin/attributes/list/')}}/'+$('select[name="parent_id"]').val(),
                    dataType: 'json',
                    processResults: function (ArrayData) {
                        return {
                            results : $.map(ArrayData, function (obj) {
                                obj.text = obj.text ||  obj.en.name+" - "+obj.ar.name; // replace name with the property used for the text
                                return obj;
                            }),
                        };
                    },
                },
            });
        });
        oldOptionsData = JSON.parse($('input[name="attributes"]').val());
        $.each(oldOptionsData,function (key,val){
            var data = {
                id: val.id,
                text: val.name
            };
            var newOption = new Option(data.text, data.id, true, true);
            $('.select2').append(newOption).trigger('change');
        });

        // var data =  $("ul.select2-selection__rendered").sortable("serialize").get();
        // var jsonString = JSON.stringify(data, null, ' ');
        // console.log(jsonString);

        $('.select2').select2().on('change',function (e){
            var selectedOptions = $(this).select2("data",e.choice);
            jsonAttributes = [];
            attributesList = [];
            $(".dd-list").empty();
            $.each(selectedOptions,function (k,item) {
                $id = item.id;
                $text = item.text;
                jsonAttributes.push({id:$id,name:$text});
                attributesList.push({id:$id});
            });
            $('input[name="attributes"]').val(JSON.stringify(attributesList));
            getOptionsFromInput(JSON.stringify(jsonAttributes));
        });

        function getOptionsFromInput(oldOptions)
        {
            jsonData = JSON.parse(oldOptions);
            $.each(jsonData,function (key,res) {
                $(".dd-list").append(
                    '<li class="dd-item" data-id="'+res.id+'">' +
                    '<div class="dd-handle" data-text="'+res.name+'" >'+res.name+'</div>' +
                    '<a class="btn btn-icon-only dd-delete-item"><i class="fa fa-trash"></i></a>' +
                    '</li>'
                );
            });
        }
        $('.dd').on('change', function() {
            jsonData = $('.dd').nestable('serialize');
            newSerializedJSON = JSON.stringify(jsonData);
            $('input[name="attributes"]').val(newSerializedJSON);
        });

        $(document).on('click','.dd-delete-item',function () {

            $(this).parent().remove();

            // var selectedStates = $('#attributes').select2("val"),
            //     index = selectedStates.indexOf('NV'); // we're removing NV, try to find it in the selected states
            // if (index > -1) {
            //     // if NV is found, remove it from the array
            //     selectedStates.splice(index, 1);
            //     // and set the value of the select to the rest of the selections
            //     $('#attributes').select2("val", selectedStates);

            var values = $('#attributes').select2('val'),
                i = values.indexOf($(this).parent().attr('data-id'));
            values.splice(i, 1);
            $('#attributes').select2().val(values).trigger('change');

        });
        @foreach(locales() as $local)
        $('input[name="{{$local->code}}[name]"]').on('change',function () {
            var string = $(this).val();
            var local  = "{{$local->code}}";
            $.get('{{url('admin/categories/slugify')}}/'+string+'/'+local,function (response) {
                $('input[name="{{$local->code}}[slug]"]').val(response.slug);
            });
        });
        $('input[name="{{$local->code}}[slug]"]').dblclick(function () {
            $(this).attr('readonly',false);
        });
        $('input[name="{{$local->code}}[slug]"]').focusout(function () {
            $(this).attr('readonly',true);
        });

        @endforeach

        $('#add-attribute').on('click',function () {
            $('#addWrapper').toggle('slow');
        })


    </script>
@stop
