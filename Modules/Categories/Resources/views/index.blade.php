@extends('admin.layouts.master')
@section('page-title','Categories')
@section('breadcrumb')
    <li class="breadcrumb-item {{($parent == null)?"active":""}}">@if($parent != null)<a href='{{url('admin/categories')}}'>Categories</a>@else Categories @endif</li>
    @php($parents = \Modules\Categories\Models\Category::getParentsFlat($parent))
    @if($parent != null)
        @foreach($parents as $id=>$name)
        <li class="breadcrumb-item {{($parent == $id)?'active':''}}">
            @if($parent != $id)
                <a href="{{url('admin/categories/'.$id)}}">{{$name}}</a>
            @else
                {{$name}}
            @endif
        </li>
        @endforeach
    @endif
@endsection
@section('content')


        <div class="card">
            <div class="header">
                @if(@$parents && count($parents)<4 || $parent == null)
                    <a href="{{url('admin/categories/create/'.$parent)}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
                @endif
                <h2>Categories list</h2>
            </div>
            <div class="body">
                @include('admin.pratical.message')
                <div class="table-responsive">
                    <table class="table table-hover m-b-0 c_list datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/categories/datatable/?parent='.$parent) !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'options', name: 'options' },
                ],
            });
        });

        function approve($id){
            $.get('{{url('dashboard/countries/flag/')}}/'+$id,function (response) {
                if(response.state == 'deapproved'){
                    $('a[data-id="'+$id+'"]').removeClass('btn-danger').addClass('btn-success').text('approve');
                }else{
                    $('a[data-id="'+$id+'"]').removeClass('btn-success').addClass('btn-danger').text('Deapprove');
                }
            });
        }
    </script>
@stop
