<?php

namespace Modules\Units\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Units\Models\Unit;

class UnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('units::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('units::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'ar.name'=>'required',
           'en.name'=>'required',
        ]);
        $unit = new Unit();
        $unit->save();
        $unit->translateAttributes($request->only(['ar','en']));
        return redirect('admin/units')->with(['success'=>'unit created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('units::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Unit $unit)
    {
        return view('units::edit',compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Unit $unit,Request $request)
    {
        $this->validate($request,[
            'ar.name'=>'required',
            'en.name'=>'required',
        ]);
        $unit->save();
        $unit->deleteTranslations();
        $unit->translateAttributes($request->only(['ar','en']));
        return redirect('admin/units')->with(['success'=>'unit updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Unit $unit)
    {
        $unit->delete();
        $unit->deleteTranslations();
        return redirect('admin/units')->with(['success'=>'unit deleted successfully']);
    }

    public function datatable()
    {
        $units = Unit::get();
        return \DataTables::of($units)
            ->editColumn('name',function ($model){
                return $model->name;
            })->editColumn('options',function ($model){
                return "<a href='".url('admin/units/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/units/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }
}
