@extends('admin.layouts.master')
@section('page-title','Edit Unit')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Edit Unit</h2>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::model($unit,['action'=>['\Modules\Units\Http\Controllers\UnitsController@update',$unit],'method'=>'PATCH'])}}
            <ul class="nav nav-tabs">
                @foreach(locales() as $k=>$local)
                    <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                @endforeach
            </ul>
            <div class="tab-content">
                @foreach(locales() as $k=>$local)
                    <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                        <div class="form-group">
                            <label>Name {{ $local->code}} </label>
                            {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
                        </div>
                    </div>
                @endforeach


                <button type="submit" class="btn btn-success">Save</button>
            </div>

            {{Form::close()}}
        </div>
    </div>
@endsection

