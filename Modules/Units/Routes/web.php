<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('admin:admin')->group(function (){
    Route::get('units/datatable', 'UnitsController@datatable');
    Route::get('units/{unit}/delete', 'UnitsController@destroy');
    Route::resource('units', 'UnitsController');
});
//Route::prefix('units')->group(function() {
//    Route::get('/', 'UnitsController@index');
//});
