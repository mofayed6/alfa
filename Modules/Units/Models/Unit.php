<?php

namespace Modules\Units\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use TranslatableTrait;
    protected $table        = 'units';
    protected $fillable     = [];
    protected $translatable = ['name'];
    protected $appends      = ['name'];

    public function getNameAttribute()
    {
        return $this->name;
    }
}
