@extends('admin.layouts.master')
@section('page-title','Create Product')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/products')}}">@yield('page-title')</a></li>
    <li class="breadcrumb-item active">Create New Product</li>
@endsection
@section('content')
    {{Form::model($product,['route'=>['products.update',$product],'method'=>'PATCH','files'=>true])}}

    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-6">
                            <h2>Create New Product </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('admin.pratical.message')
                    <div class="table-responsive">
                        <ul class="nav nav-tabs">
                            @foreach(locales() as $k=>$local)
                                <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach(locales() as $k=>$local)
                                <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Name {{ $local->code}} </label>
                                                {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Description {{ $local->code}} </label>
                                                {{Form::textarea($local->code.'[description]',null,['class'=>'form-control','rows'=>3])}}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Tags {{ $local->code}} </label>
                                                {{Form::textarea($local->code.'[tags]',null,['class'=>'form-control','rows'=>3])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Barcode</label>
                                        {{Form::number('barcode',null,['class'=>'form-control'])}}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Preparation Duration (minutes)</label>
                                        {{Form::number('preparation_duration',null,['class'=>'form-control'])}}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Images </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Featured Image</label>
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{url('uploads/'.$product->featured_image)}}" alt="" class="img-thumbnail">
                            </div>
                        </div>
                        {{Form::file('featured_image',['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Images</label>
                        <div class="row">
                            {{Form::hidden('uploadedFiles')}}
                            <div class="col-md-12">
                                <div class="file-loading">
                                    <input id="input-pr" name="images[]" type="file" data-allowed-file-extensions='["png", "jpg","jpeg"]'  accept="image/*"  multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Main Features</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Unit</label>
                        {{Form::select('unit',$units,null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Unit Volume</label>
                        {{Form::number('unit_volume',null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Identifier</label>
                        {{Form::select('identifier_id',$identifiers,null,['class'=>'form-control','placeholder'=>'select identifier ...'])}}
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        {{Form::select('category_id',$categories,null,['class'=>'form-control','id'=>'category'])}}
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Attribute</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group" id="att">
                    </div>
                    <div id="test"></div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Pricing </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Price</label>
                        {{Form::number('price',null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Discount Price</label>
                        {{Form::number('discount_price',null,['class'=>'form-control'])}}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Classifications </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @foreach($classifications as $classification)
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="fancy-checkbox">
                                    {{Form::checkbox('classifications[]',$classification->id)}}
                                    <span>{{$classification->title}}</span>
                                </label>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Save </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-block"> Save </button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-info btn-block"> Cancel </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{Form::close()}}

@endsection
@section('styles')
    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />

@endsection

@section('scripts')
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/piexif.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/sortable.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/purify.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/fileinput.min.js')}}" type="text/javascript"></script>
    <script>

        var urls =[
            @foreach($product->images as $image)
                "{{url('uploads/'.$image->image)}}",
            @endforeach
        ];

        $("#input-pr").fileinput({
            uploadUrl: "{{route('product.images.upload')}}",
            deleteUrl: "{{route('product.images.delete')}}",
            uploadAsync: false,
            maxFileCount: 10,
            overwriteInitial: false,
            initialPreview: urls,
            initialPreviewAsData: true, // allows you to set a raw markup
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            initialPreviewDownloadUrl: "{{url('uploads/')}}/{key}", // includes the dynamic key tag to be replaced for each config
            initialPreviewConfig: [
                @foreach($product->images as $image)
                    {type: "image", caption: "Image-2.jpg", size: 817000,  key: "{{$image->image}}" },  // set as raw markup
                @endforeach
            ],
            uploadExtraData: {
                _token: "{{csrf_token()}}",
            },
            deleteExtraData: {
                _token: "{{csrf_token()}}"
            }
        }).on('filesorted', function(e, params) {
            console.log('File sorted params', params);
        }).on('fileuploaded', function(e, params) {
            console.log('File uploaded params', params);
        });



      $(document).ready(function () {

$("#category").on('change', function() {
    //alert($(this).val());
    $( "#att" ).html("");
    $( "#test" ).html("");

    $.ajax({
        url: "{{ url('admin/products/attribute/') }}" + '/' +$(this).val(),
        type: 'GET',
        success: function (result) {


            $.map( result, function( n ) {

                $("#test").append("<input type='hidden' name='attribute_id[]' id='attribute_id"+ n.id +"'> ");
                var input = $( "#attribute_id" + n.id ).val(n.id);

                if(n.template === 'input_text'){

                    $( "#att" ).append( " <label>"+ n.en.name +"</label><input class='form-control' type='text' name='attribute_product[]'><br>" );
                }else{

                    var select = "<label>"+ n.en.name +"</label><select class='form-control' name='attribute_product[]'>";


                    $.each( n.en.options, function( e ,v) {

                        select = select + '<option>' + v.name + '</option>';

                    });

                    select = select + "</select>"

                    $( "#att" ).append(select);
                }

            });
        }
    });
});
});



    </script>
@endsection
