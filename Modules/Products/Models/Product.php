<?php

namespace Modules\Products\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Classifications\Models\Classification;
use Modules\Attributes\Models\Attribute;
class Product extends Model
{
    use TranslatableTrait;
    protected $fillable = [];
    protected $translatable = ['name','description','tags'];
    protected $appends = ['name'];


    public function classifications()
    {
        return $this->belongsToMany(Classification::class,'product_classifications','product_id','classification_id');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class,'product_id');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'attributes_product','product_id','attribute_id')->withPivot('value');
    }

    public function getNameAttribute()
    {
        return $this->name;
    }


}
