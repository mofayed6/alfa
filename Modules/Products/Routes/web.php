<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('admin:admin')->group(function (){

    Route::group(['prefix'=>'products/images'],function (){
        Route::post('upload', 'ProductsController@uploadImages')->name('product.images.upload');
        Route::post('delete', 'ProductsController@deleteImage')->name('product.images.delete');

    });

    Route::get('products/datatable', 'ProductsController@datatable');


    Route::get('products/{product}/delete', 'ProductsController@destroy');

    Route::get('products/attribute/{id}', 'ProductsController@attributeCategory');


    Route::resource('products', 'ProductsController');



});

