<?php

namespace Modules\Products\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attributes\Models\Attribute;
use Modules\Categories\Models\Category;
use Modules\Classifications\Models\Classification;
use Modules\Identifiers\Models\Identifier;
use Modules\Products\Models\Product;
use Modules\Products\Models\ProductImage;
use Modules\Units\Models\Unit;
use DB;


class ProductsController extends Controller
{

    protected $list = [];
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('products::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories     = Category::getChildrenFlat();
        $units          = Unit::all()->pluck('name','id');
        $classifications= Classification::all();
        $identifiers    = Identifier::pluck('identifier','id');
        return view('products::create',compact('categories','units','classifications','identifiers'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $this->validate($request,[
            'en.name'=>'required',
            'ar.name'=>'required',
            'barcode'=>'required',
            'preparation_duration'=>'required',
            'unit'=>'required',
            'unit_volume'=>'required',
            'category_id'=>'required',
            'price'=>'required',
            'discount_price'=>'required',
            'featured_image'=>'required|image',
        ]);

        $product = new Product();
        $product->barcode               = $request->barcode;
        $product->preparation_duration  = $request->preparation_duration;
        $product->unit_id               = $request->unit;
        $product->unit_volume           = $request->unit_volume;
        $product->category_id           = $request->category_id;
        $product->price                 = $request->price;
        $product->discount_price        = $request->discount_price;
        $product->identifier_id         = $request->identifier_id;
        $product->featured_image        = $request->featured_image->store('products');
        $product->save();
        $product->translateAttributes($request->only('ar','en'));
        if($request->filled('uploadedFiles')){
            $files = json_decode($request->get('uploadedFiles'));
            ProductImage::whereIn('id',$files)->update(['product_id'=>$product->id]);
        }

        $product->classifications()->sync($request->classifications);
       // $pivotData = array($request->attribute_id => array('value' => $request->attribute_product));
       $data = [];
       foreach( $request->attribute_id as $k=>$t){
             $data[$t] = [ 'value'=> $request->attribute_product[$k]];
       }

        $product->attributes()->sync($data);

        return redirect('admin/products')->with(['success'=>'product created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('products::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Product $product)
    {
        $categories     = Category::getChildrenFlat();
        $units          = Unit::all()->pluck('name','id');
        $classifications= Classification::all();
        $identifiers    = Identifier::pluck('identifier','id');
        return view('products::edit',compact('categories','units','classifications','product','identifiers'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,Product $product)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'ar.name'=>'required',
            'barcode'=>'required',
            'preparation_duration'=>'required',
            'unit'=>'required',
            'unit_volume'=>'required',
            'category_id'=>'required',
            'price'=>'required',
            'discount_price'=>'required',
            'featured_image'=>'image',
        ]);

        $product->barcode               = $request->barcode;
        $product->preparation_duration  = $request->preparation_duration;
        $product->unit_id               = $request->unit;
        $product->unit_volume           = $request->unit_volume;
        $product->category_id           = $request->category_id;
        $product->price                 = $request->price;
        $product->discount_price        = $request->discount_price;
        $product->identifier_id         = $request->identifier_id;
        $product->featured_image        = ($request->hasFile('featured_image'))?$request->featured_image->store('products'):$product->featured_image;
        $product->save();
        $product->deleteTranslations();
        $product->translateAttributes($request->only('ar','en'));
        if($request->filled('uploadedFiles')){
            $files = json_decode($request->get('uploadedFiles'));
            ProductImage::whereIn('id',$files)->update(['product_id'=>$product->id]);
        }
        $product->classifications()->sync($request->classifications);
        
        $data = [];
        foreach( $request->attribute_id as $k=>$t){
              $data[$t] = [ 'value'=> $request->attribute_product[$k]];
        }

        $product->attributes()->detach();

        $product->attributes()->attach($data);
         
       //  $product->attributes()->attach($data);

         return redirect('admin/products')->with(['success'=>'product updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        $product->deleteTranslations();
        return redirect('admin/products')->with(['success'=>'product deleted successfully']);

    }

    public function uploadImages(Request $request)
    {
        $this->validate($request,[
            'images'    => 'required',
            'images.*'  => 'image',
        ]);
        $res = [];
        foreach ($request->images as $k=> $image){
            $img = new ProductImage();
            $img->image         = $image->store('products');
            $img->product_id    = null;
            $img->save();
            $res=$img->id;
        }
        return response()->json($res);
    }

    public function deleteImage(Request $request)
    {
        ProductImage::where('image',$request->key)->delete();
        return response()->json(['success'=>'true']);
    }

    public function datatable()
    {
        $locations = Product::query();
        return \DataTables::eloquent($locations)
            ->addColumn('name',function ($model){
                return $model->name;
            })->editColumn('options',function ($model){
                return "<a href='".url('admin/products/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/products/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }



    public function chechRecusive($father)
    {

        if ($father->parentRecursive   === null)
        {
            return $this->list;

        }else{
            $this->list[] = $father->parentRecursive->id;
            $this->chechRecusive($father->parentRecursive);
        }
    }

    public function attributeCategory($parent_id)
    {

        $fathers =  Category::with('parentRecursive')->where('id', $parent_id)->first();

        $this->chechRecusive ($fathers);

        array_push($this->list,$parent_id);

        $attributesID = \DB::table('category_attributes')->whereIn('category_id',$this->list)->pluck('attribute_id');
        $attributes = Attribute::find($attributesID);
        return $attributes;


    }
}
