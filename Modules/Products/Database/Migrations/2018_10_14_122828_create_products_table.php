<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('unit_id');
            $table->unsignedInteger('identifier_id')->nullable();
            $table->integer('barcode');
            $table->integer('liked_to');
            $table->integer('unit_volume')->nullable();
            $table->integer('preparation_duration')->nullable();
            $table->decimal('price');
            $table->decimal('discount_price')->nullable();
            $table->string('featured_image',255)->nullable();
            $table->timestamps();
        });
        //set integers length
        Schema::table('products', function (Blueprint $table) {
            DB::statement('ALTER TABLE '.$table->getTable().' MODIFY COLUMN brand_id INT(4)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN category_id INT(3)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN barcode INT(16)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN unit_id INT(2)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN unit_volume INT(5)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN preparation_duration INT(4)');
            //foreign key constrains
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
            $table->foreign('identifier_id')->references('id')->on('identifiers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
