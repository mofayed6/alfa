<?php

namespace Modules\Admins\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Rules\Models\Rule;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $table ='admins';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rule()
    {
        return $this->belongsTo(Rule::class,'rule_id');
    }
}
