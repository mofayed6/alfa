<?php

namespace Modules\Admins\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admins\Models\Admin;
use Modules\Rules\Models\Rule;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admins::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $rules = Rule::get()->pluck('en.name','id');
        return view('admins::create',compact('rules'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name'   =>'required',
           'rule_id'=>'required',
           'email'  =>'required|email|unique:admins',
           'password'=>'required|min:6',
        ]);
        $admin = new Admin();
        $admin->name    = $request->name;
        $admin->email   = $request->email;
        $admin->password= \Hash::make($request->password);
        $admin->rule_id = $request->rule_id;
        $admin->save();
        return redirect('admin/admins')->with(['success'=>'admin created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admins::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Admin $admin)
    {
        $rules = Rule::get()->pluck('en.name','id');
        return view('admins::edit',compact('admin','rules'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,Admin $admin)
    {
        $this->validate($request,[
            'name'   =>'required',
            'rule_id'=>'required',
            'email'  =>'required|email|unique:admins,email,'.$admin->id,
        ]);
        if($request->filled('password')){
            $this->validate($request,['password'=>'min:6']);
        }
        $admin->name    = $request->name;
        $admin->email   = $request->email;
        $admin->password= ($request->filled('password'))?\Hash::make($request->password):$admin->password;
        $admin->rule_id = $request->rule_id;
        $admin->save();
        return redirect('admin/admins')->with(['success'=>'admin updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();
        return redirect('admin/admins')->with(['success'=>'admin deleted successfully']);
    }


    public function datatable()
    {
        $admin = Admin::query();
        return \DataTables::eloquent($admin)
            ->addColumn('rule',function ($model){
                return $model->rule->name;
            })->addColumn('options',function ($model){

                if(auth('admin')->user()->rule_id === 1 ){
                    return "<a href='".url('admin/admins/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                     <a href='".url('admin/admins/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";
                }



            })
            ->make();

    }


}
