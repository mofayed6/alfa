<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rule_id');
            $table->string('name',60);
            $table->string('email',60);
            $table->string('password',60);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('admins', function (Blueprint $table) {
            //set integers length
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN id INT(5) AUTO_INCREMENT');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN rule_id INT(2)');
            //foreign key constrains
            $table->foreign('rule_id')->references('id')->on('rules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
