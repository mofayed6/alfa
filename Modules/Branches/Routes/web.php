<?php

Route::prefix('branches')->middleware('admin:admin')->group(function() {
    Route::get('/', 'BranchesController@index');
    Route::get('create', 'BranchesController@create');
    Route::post('/', 'BranchesController@store');
    Route::get('{branch}/edit', 'BranchesController@edit');
    Route::get('{branch}/delete', 'BranchesController@destroy');
    Route::patch('{branch}', 'BranchesController@update');
    Route::get('datatable', 'BranchesController@datatable');
});
