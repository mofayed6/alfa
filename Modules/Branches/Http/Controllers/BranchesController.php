<?php

namespace Modules\Branches\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Branches\Models\Branch;
use App\Http\Controllers\Controller;
use Modules\Locations\Models\Location;

class BranchesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('branches::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $locations = Location::getAll();
        return view('branches::create',compact('locations'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'en.address'=>'required',
            'ar.name'=>'required',
            'ar.address'=>'required',
            'country_id'=>'required',
            'city_id'=>'required',
            'district_id'=>'required',
        ]);
        $branch = new Branch();
        $branch->phone          = $request->phone;
        $branch->country_id     = $request->country_id;
        $branch->city_id        = $request->city_id;
        $branch->district_id    = $request->district_id;
        $branch->latitude       = $request->lat;
        $branch->longitude      = $request->lng;
        $branch->save();
        $branch->translateAttributes($request->only(['ar','en']));
        return redirect('admin/branches')->with(['success'=>'branch created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('branches::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Branch $branch)
    {
        $locations = collect(Location::getAll());
        return view('branches::edit',compact('branch','locations'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Branch $branch,Request $request)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'en.address'=>'required',
            'ar.name'=>'required',
            'ar.address'=>'required',
            'country_id'=>'required',
            'city_id'=>'required',
            'district_id'=>'required',
        ]);
        $branch->phone          = $request->phone;
        $branch->country_id     = $request->country_id;
        $branch->city_id        = $request->city_id;
        $branch->district_id    = $request->district_id;
        $branch->latitude       = $request->lat;
        $branch->longitude      = $request->lng;
        $branch->save();
        $branch->deleteTranslations();
        $branch->translateAttributes($request->only(['ar','en']));
        return redirect('admin/branches')->with(['success'=>'branch updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();
        $branch->deleteTranslations();
        return redirect('admin/branches')->with(['success'=>'branch deleted successfully']);

    }

    public function datatable()
    {
        $locations = Branch::get();
        return \DataTables::of($locations)
            ->editColumn('name',function ($model){
                return $model->name;
            })->editColumn('options',function ($model){
                return "<a href='".url('admin/branches/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/branches/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }
}
