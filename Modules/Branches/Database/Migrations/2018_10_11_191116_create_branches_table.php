<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('latitude','10','8');
            $table->decimal('longitude','11','8');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('district_id');
            $table->string('phone',11);
            $table->timestamps();
        });

        Schema::table('branches', function (Blueprint $table) {
            //set integers length
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN id INT(4) AUTO_INCREMENT');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN country_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN city_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN district_id INT(7)');
            //foreign key constrains
            $table->foreign('country_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('district_id')->references('id')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
