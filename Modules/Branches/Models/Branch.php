<?php

namespace Modules\Branches\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Locations\Models\Location;

class Branch extends Model
{
    use TranslatableTrait;
    protected $table        = 'branches';
    protected $fillable     = ['latitude', 'longitude', 'country_id', 'city_id', 'district_id', 'phone'];
    protected $translatable = ['name','address'];
    protected $appends = ['name'];

    public function country()
    {
        return $this->belongsTo(Location::class,'country_id');
    }
    public function city()
    {
        return $this->belongsTo(Location::class,'city_id');
    }
    public function district()
    {
        return $this->belongsTo(Location::class,'district_id');
    }

    public function getNameAttribute()
    {
        return $this->name;
    }

}
