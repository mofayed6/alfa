@extends('admin.layouts.master')
@section('page-title','Create Branch')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/branches')}}">@yield('page-title')</a></li>
    <li class="breadcrumb-item active">Create New Branch</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Create New Branch </h2>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <ul class="nav nav-tabs">
                    @foreach(locales() as $k=>$local)
                        <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                    @endforeach
                </ul>
                {{Form::open(['action'=>'\Modules\Branches\Http\Controllers\BranchesController@store'])}}
                <div class="tab-content">
                    @foreach(locales() as $k=>$local)
                        <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                           <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name {{ $local->code}} </label>
                                        {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Address {{ $local->code}} </label>
                                        {{Form::textarea($local->code.'[address]',null,['class'=>'form-control','rows'=>3])}}
                                    </div>
                                </div>
                           </div>
                        </div>
                    @endforeach
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Phone</label>
                                {{Form::text('phone',null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Country</label>
                                {{Form::select('country_id',[],null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>City</label>
                                {{Form::select('city_id',[],null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>District</label>
                                {{Form::select('district_id',[],null,['class'=>'form-control'])}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                            <input id="pac-input" class="controls form-control" type="text" placeholder="Search Box">
                            <div id="map"></div>
                                {{Form::hidden('lat',null,['id'=>'lat'])}}
                                {{Form::hidden('lng',null,['id'=>'lng'])}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"> Save </button>
                            </div>
                        </div>

                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
@section('styles')
    <style>
        #pac-input,.pac-card{background-color:#fff}#map{height:600px}body,html{height:100%;margin:0;padding:0}#description{font-family:Roboto;font-size:15px;font-weight:300}#infowindow-content .title{font-weight:700}#pac-input,.pac-controls label{font-family:Roboto;font-weight:300}#infowindow-content{display:none}#map #infowindow-content{display:inline}.pac-card{margin:10px 10px 0 0;border-radius:2px 0 0 2px;box-sizing:border-box;-moz-box-sizing:border-box;outline:0;box-shadow:0 2px 6px rgba(0,0,0,.3);font-family:Roboto}#pac-container{padding-bottom:12px;margin-right:12px}.pac-controls{display:inline-block;padding:5px 11px}.pac-controls label{font-size:13px}#pac-input{font-size:15px;margin-left:-190px;padding:0 11px 0 13px;text-overflow:ellipsis;z-index: 9999 !important;}#pac-input:focus{border-color:#4d90fe}#title{color:#fff;background-color:#4d90fe;font-size:25px;font-weight:500;padding:6px 12px}#target{width:345px}
    </style>
@endsection
@section('scripts')
    <script>
        var allLocations    = @json($locations);
        var cities          = null;
        var districts       = null;
        fillOptionsFromList(allLocations,$('select[name="country_id"]'),{{old('country_id')}});

        $(document).ready(function () {
            cities      = getById(allLocations,{{old('country_id')}}).cities;
            fillOptionsFromList(cities,$('select[name="city_id"]'),{{old('city_id')}});

            districts = getById(cities,{{old('city_id')}}).districts;
            fillOptionsFromList(districts,$('select[name="district_id"]'),{{old('district_id')}});

        });

        $('select[name="country_id"]').on('change',function () {
            districts       = null;
            cities          = null;

            if($(this).val() === ""){
                districts   = null;
                cities      = null
            }else{
                cities      = getById(allLocations,$(this).val()).cities;
            }

            fillOptionsFromList(cities,$('select[name="city_id"]'));
            fillOptionsFromList(districts, $('select[name="district_id"]'));
        });

        $('select[name="city_id"]').on('change',function () {
            districts = getById(cities,$(this).val()).districts;
            fillOptionsFromList(districts,$('select[name="district_id"]'));
        });

        function getById($data,$id) {
            return getBy($data,'id',$id);
        }
        function getBy($data,$key,$value)
        {
            return $.grep( $data, function( n, i ) {
                return n['id']==$value;
            })[0];
        }

        function fillOptionsFromList($list,$slector,$selectedId)
        {
            $selectedId = $selectedId || 0;
            $slector.empty();
            if($list && $list.length != 0)
                $slector.append('<option value>Please Select</option>');

            $.each($list,function (items,$item) {
                selected = ($item.id===$selectedId)?"selected":"";
                $slector.append('<option value="'+$item.id+'" '+selected+' >'+$item.name+'</option>')
            });
        }




        var map = null;
        var marker = null;
        function initAutocomplete() {
            var mapProp = {
                zoom: 12,
                center: new google.maps.LatLng({lat:30.0330459, lng:31.2104018 }),
                mapTypeId: 'roadmap',
            };
            map = new google.maps.Map(document.getElementById('map'), mapProp);
            /*var styles = [{stylers: [{hue: "#c5c5c5"}, {saturation: -100}]},];
            map.setOptions({styles: styles});*/
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            map.addListener('click', function(event) {
                //call function to create marker
                if (marker) {
                    marker.setMap(null);
                    marker = null;
                }
                marker = createMarker(event.latLng, "name", "<b>Location</b><br>"+event.latLng);

            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };



                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });

        }


        function createMarker(latlng, name, html) {
            var contentString = html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                zIndex: Math.round(latlng.lat()*-100000)<<5
            });
            google.maps.event.trigger(marker, 'click');
            $('#lat').val(latlng.lat());
            $('#lng').val(latlng.lng());
            return marker;
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAoacCf8RDzMIaHcI5Ywh9zQ-kZt1-V_fc&libraries=places&callback=initAutocomplete" async defer></script>
@stop
