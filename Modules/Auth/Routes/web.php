<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Route::get('login', 'AdminAuth@login')->name('login');
    Route::post('login', 'AdminAuth@dologin');
    $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'ResetPasswordController@reset')->name('password.update');
//

    Route::group(['middleware' => 'admin:admin'], function () {

        Route::get('/', function () {
            return view('admin.home');
        })->name('home');

        Route::any('logout', 'AdminAuth@logout')->name('doLogout');


        //Route::resource('admin', 'AdminController');
    });


});

