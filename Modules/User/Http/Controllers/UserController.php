<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Languages\Models\Local;
use Modules\User\Models\User;
use \App\Http\Controllers\Controller;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('user::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    public function create()
    {

        return view('user::create');
    }


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_name'=>'required',
            'last_name'=>'required',
            'gender'=>'required',
            'date_of_birth'=>'required',
            'mobile'=>'required',
            'home_number'=>'required',
            'email'=>'required',
            'status'=>'required',
        ]);

        $new_user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'date_of_birth' => $request->date_of_birth,
            'mobile' => $request->mobile,
            'home_number' => $request->home_number,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'status' => $request->status,
        ]);

        return redirect(url('admin/user/'))->with(['success'=>'User Created Successfully']);


    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('user::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('user::edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $user = User::findOrFail($id);

        $this->validate($request,[
            'first_name'=>'required',
            'last_name'=>'required',
            'gender'=>'required',
            'date_of_birth'=>'required',
            'mobile'=>'required',
            'home_number'=>'required',
            'email'=>'required',
            'status'=>'required',
        ]);

        $new_user = $user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'gender' => $request->gender,
            'date_of_birth' => $request->date_of_birth,
            'mobile' => $request->mobile,
            'home_number' => $request->home_number,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'status' => $request->status,
        ]);

        return redirect(url('admin/user'))->with(['success'=>'User Updated Successfully']);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $user = User::findOrFail($id);

        if ($user->delete()) {
            return redirect()->back()->with(['success'=>'Data was deleted successfully']);
        } else {
            return redirect()->back()->with(['error'=>'failed to delete data, please try again later']);
        }

    }


    public function datatable(Request $request)
    {
        $users = User::get();
        return \DataTables::of($users)
            ->editColumn('name',function ($model){
                return $model->first_name. " " .$model->last_name;
            })
            ->editColumn('gender',function ($model){
                if($model->gender == 'male'){
                    return "Male";
                }else{
                    return "Female";
                }
            })
            ->editColumn('status',function ($model){
                if($model->status == 1){
                    return "<span class='badge badge-success'>Active</span>";

                }elseif($model->status == 2)
                {
                   return '<span class="badge badge-default">Disabled</span>';

                }else{
                    return '<span class="badge badge-warning">Blocked</span>';
                }
            })
            ->editColumn('options',function ($model){
                return "<a href='".url('admin/user/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a> 
                        <a href='".url('admin/user/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>  ";
            })
            ->make(true);
    }

}
