<?php

namespace Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\DeliveryLocation\Models\UserAddress;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $fillable = ['first_name', 'last_name', 'gender', 'date_of_birth', 'mobile', 'home_number', 'email', 'status'];


    public function UserAddress ()
    {
        return  $this->hasMany(UserAddress::class);
    }

}
