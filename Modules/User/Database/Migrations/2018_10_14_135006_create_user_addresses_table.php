<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('district_id');
            $table->string('address', 300);
            $table->integer('apartment_number')->nullable();
            $table->string('compound_name',60)->nullable();
            $table->string('notes',300)->nullable();
            $table->boolean('is_default')->default(0)->comment('0=>not default | 1=> Default');
            $table->timestamps();
        });
        Schema::table('user_addresses', function (Blueprint $table) {
            //set integers length
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN apartment_number INT(4)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN country_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN city_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN district_id INT(7)');
            //foreign key constrains
            $table->foreign('country_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('district_id')->references('id')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
