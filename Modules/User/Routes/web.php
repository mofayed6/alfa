<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('admin:admin')->prefix('admin/user')->group(function() {

    Route::get('datatable','UserController@datatable');
    Route::get('create/{parent?}', 'UserController@create');
    Route::get('/{user}/edit', 'UserController@edit');
    Route::patch('/{user}/update', 'UserController@update');
    Route::get('/{parent?}', 'UserController@index');
    Route::post('/','UserController@store');
    Route::get('{id}/delete','UserController@destroy');

});
