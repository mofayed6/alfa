@extends('admin.layouts.master')
@section('page-title','New User')
@section('breadcrumb')
    <li class="breadcrumb-item"><a href='{{url('admin/user')}}'>Users</a> </li>

        <li class="breadcrumb-item active">Create New</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <h2>Create New User</h2>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            {{Form::open(['action'=>'\Modules\User\Http\Controllers\UserController@store'])}}
                @include('user::form')
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/user/datatable') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'first_name', name: 'first_name' },
                    { data: 'last_name', name: 'last_name' },
                    { data: 'gender', name: 'gender' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'status', name: 'status' },
                ],
            });
        });





    </script>
@stop
