
<div class="tab-content">
    <div class="form-group">
        <label> first name </label>
        {{ Form::text('first_name',null,array('class'=>'form-control')) }}

    </div>

    <div class="form-group">
        <label> last name </label>
        {{ Form::text('last_name',null,array('class'=>'form-control')) }}
    </div>


    <div class="form-group">
        <label>date of birth  </label>
        {{ Form::date('date_of_birth',null,array('class'=>'form-control')) }}
    </div>

    <div class="form-group">
        <label> mobile </label>
        {{ Form::text('mobile',null,array('class'=>'form-control')) }}
    </div>

    <div class="form-group">
        <label> home number </label>
        {{ Form::text('home_number',null,array('class'=>'form-control')) }}
    </div>

    <div class="form-group">
        <label> Email </label>
        {{ Form::email('email',null,array('class'=>'form-control')) }}
    </div>
    <div class="form-group">
        <label> Password </label>
        {{ Form::password('password',array('class'=>'form-control','required')) }}
    </div>


    <div class="form-group">

        <label class="fancy-radio">
            {{Form::radio('gender','male',null)}}
            <span><i></i>Male</span>
        </label>

        <label class="fancy-radio">
            {{Form::radio('gender','female',null)}}
            <span><i></i>Female</span>
        </label>
    </div>


    <div class="form-group">

        <label class="fancy-radio">
            {{Form::radio('status',1,null)}}
            <span><i></i>Active</span>
        </label>

        <label class="fancy-radio">
            {{Form::radio('status',2,null)}}
            <span><i></i>Disabled</span>
        </label>

        <label class="fancy-radio">
            {{Form::radio('status',3,null)}}
            <span><i></i>Blocked</span>
        </label>
    </div>




    <button type="submit" class="btn btn-success">Save</button>

</div>
