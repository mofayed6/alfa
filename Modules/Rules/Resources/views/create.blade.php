@extends('admin.layouts.master')
@section('page-title','Create New Rule')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/rules')}}">Admin Rules</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Create New Branch </h2>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <ul class="nav nav-tabs">
                    @foreach(locales() as $k=>$local)
                        <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                    @endforeach
                </ul>
                {{Form::open(['action'=>'\Modules\Rules\Http\Controllers\RulesController@store'])}}
                    @include('rules::form')
                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
