<?php

namespace Modules\Rules\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Rules\Models\Permission;
use Modules\Rules\Models\Rule;

class RulesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('rules::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $permissions = Permission::all();
        return view('rules::create',compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'en.name'       =>'required',
            'ar.name'       =>'required',
            'permissions'   =>'required',
        ]);
        $rule = new Rule;
        $rule->save();
        $rule->translateAttributes($request->only('ar','en'));
        $rule->permissions()->sync($request->permissions);
        return redirect('admin/rules')->with(['success'=>'Rule Created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('rules::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Rule $rule)
    {
        $permissions = Permission::all();
        return view('rules::edit',compact('rule','permissions'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,Rule $rule)
    {
        $this->validate($request,[
            'en.name'       =>'required',
            'ar.name'       =>'required',
            'permissions'   =>'required',
        ]);
        $rule->save();
        $rule->deleteTranslations();
        $rule->translateAttributes($request->only('ar','en'));
        $rule->permissions()->sync($request->permissions);
        return redirect('admin/rules')->with(['success'=>'Rule Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Rule $rule)
    {
        $rule->permissions()->detach();
        $rule->delete();
        $rule->deleteTranslations();
        return redirect('admin/rules')->with(['success'=>'Rule Delete Successfully']);
    }

    public function datatable()
    {
        $rules = Rule::query();
        return \DataTables::eloquent($rules)
            ->addColumn('name',function ($model){
                return $model->name;
            })->addColumn('options',function ($model){
                return "<a href='".url('admin/rules/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/rules/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make();
    }
}
