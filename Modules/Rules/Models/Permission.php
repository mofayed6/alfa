<?php

namespace Modules\Rules\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [];

    public function permissions()
    {
        return $this->belongsToMany(Rule::class,'rule_permissions','permission_id','rule_id');
    }
}
