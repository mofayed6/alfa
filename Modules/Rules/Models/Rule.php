<?php

namespace Modules\Rules\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    use TranslatableTrait;


    protected $fillable = [];
    protected $translatable = ['name'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'rule_permissions','rule_id','permission_id');
    }
}
