<?php

namespace Modules\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\OrderProduct;
use Modules\Products\Models\Product;


class ProductOrderController extends Controller
{

    public function indexOrder($id)
    {
        $porducts = Order::product($id);
        return view('orders::productOrder',compact('porducts','id'));
    }


    public function createProduct($id)
    {
        $poducts = Product::all()->pluck('name','id');
        $lastOrder= Order::lastOrderAdd()->id;
        return view('orders::createProduct',compact('lastOrder','poducts','id'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeProductOrder(Request $request, $id)
    {

       $request->validate([
            'product_id'=>'required',
            'price'=>'required',
            'quantity'=>'required',
        ]);


        $newProduct = new OrderProduct();

        $date = explode(' - ', $request->get('date'));

        $newProduct->product_id     = $request->get('product_id');
        $newProduct->order_id       = $id;
        $newProduct->price          = $request->get('price');
        $newProduct->discount_price = $request->get('discount_price');
        $newProduct->quantity       = $request->get('product_id');
        $newProduct->discount_from  = $date['0'];
        $newProduct->discount_to    = $date['1'];
        $newProduct->save();

        return redirect('admin/orders/'.$id)->with(['success'=>'ProductOrder created successfully']);

    }




    public function editOrderProduct($id,$OProductId)
    {

        $orderProduct =  OrderProduct::where('order_id',$id)->where('product_id',$OProductId)->first();
        $poducts = Product::all()->pluck('name','id');
        $lastOrder= Order::lastOrderAdd()->id;
        return view('orders::editOrderProduct',compact('poducts','lastOrder','orderProduct','id'));
    }


    public function updateOrderProduct(Request $request,$id)
    {
        $product = OrderProduct::find($id);

        $request->validate([
            'product_id'=>'required',
            'price'=>'required',
            'discount_price'=>'required',

        ]);

        $date = explode(' - ', $request->get('date'));

        $product->product_id     = $request->get('product_id');
        $product->price          = $request->get('price');
        $product->discount_price = $request->get('discount_price');
        $product->quantity       = $request->get('quantity');
        $product->discount_from  = $date['0'];
        $product->discount_to    = $date['1'];

        $product->update();
        return redirect('admin/orders')->with(['success'=>'order Updated successfully']);

    }


    public function destroyOrderProduct($id,$OProductId)
    {
        $orderProduct =  OrderProduct::where('order_id',$id)->where('product_id',$OProductId)->first();
        $orderProduct->delete();
        return redirect('admin/orders')->with(['success'=>'order Updated successfully']);

    }



    public function datatableProduct($id)
    {
        $orders = OrderProduct::whereOrderId($id)->get();
        return \DataTables::of($orders)
            ->editColumn('product',function ($model){
                   $product = Product::find($model->product_id);

                   return $product->name;

            })
            ->editColumn('options',function ($model){
                return "<a href='".url('admin/orders/'.$model->order_id.'/'.$model->product_id.'/order-product')."' class='btn btn-warning'>Edit</a>

                <a href='".url('admin/orders/'.$model->order_id.'/'.$model->product_id.'/delete')."' class='btn btn-danger'>Delete</a>";


            })
            ->make(true);
    }
}
