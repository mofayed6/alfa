<?php

namespace Modules\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Branches\Models\Branch;
use Modules\Coupons\Models\Coupon;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\OrderStatus;
use Modules\Products\Models\Product;
use Modules\User\Models\User;
use Session;
use DB;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        //dd(Order::count());
//        dd(Order::lastOrderAdd());
        return view('orders::index');
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

        $branchs = Branch::all()->pluck('name','id');
        $poducts = Product::all()->pluck('name','id');
        $coupones = Coupon::all()->pluck('code','id');
        $users = User::get();
        $status = OrderStatus::status();
        return view('orders::create',compact('branchs','coupones','users','poducts','status'));
    }


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'tex_percentage'=>'required',
            'branch_id'=>'required',
            'user_id'=>'required',
         //   'status_id'=>'required',

        ]);

        $order = new Order();
        $order->tex_percentage = $request->tex_percentage;
        $order->branch_id      = $request->branch_id;
        $order->user_id        = $request->user_id;
        $order->coupon_id      = $request->coupon_id;
//        $order->orderStatus()->associate($request->status_id);
       // $order->orderStatus()->sync($request->status_id);
         $order->save();
         $order->orderStatus()->create(['status' => $request->status_id]);

        return redirect('admin/orders')->with(['success'=>'order created successfully']);
    }


    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Order $order)
    {
        $branchs = Branch::all()->pluck('name','id');
        $poducts = Product::all()->pluck('name','id');
        $coupones = Coupon::all()->pluck('code','id');
        $users = User::get();
        $status = OrderStatus::status();
        return view('orders::edit',compact('branchs','poducts','coupones','users','order','status'));
    }


    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'tex_percentage'=>'required',
            'branch_id'=>'required',
            'user_id'=>'required',

        ]);
        $order = Order::find($id);

        $order->tex_percentage  = $request->tex_percentage ;
        $order->branch_id       = $request->branch_id ;
        $order->user_id         = $request->user_id ;
        $order->coupon_id       = $request->coupon_id ;
        $order->update();

        $order->orderStatus()->delete();
        $order->orderStatus()->create(['status' => $request->status_id]);
        return redirect('admin/orders')->with(['success'=>'order Updated successfully']);


    }


    /**
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $order = Order::find($id);
        $order->delete();
       // $order->deleteTranslations();
        return redirect('admin/orders')->with(['success'=>'product deleted successfully']);
    }


    public function productPrice($id)
    {
        $product = Product::find($id);
        return $product->price;
    }





    public function datatable()
    {
        $orders = Order::query();
        return \DataTables::eloquent($orders)
            ->editColumn('branch_id',function ($model){
                $branchs = Branch::all()->where('id',$model->branch_id)->pluck('name')->first();
                return  "<a href='".url('admin/orders/'.$model->id)."' >$branchs</a>";

            })
            ->addColumn('options',function ($model){
                return "<a href='".url('admin/orders/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/orders/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }


    public function getCount(Request $request)
    {
       Order::setSession();
       return  $value =  Session::get('orderIdCount');
    }

    public function countOrderDatabase()
    {
        $value = Order::count();
        return $value;
    }



   public function ChangeDelevary()
   {
       $orders = \Modules\Orders\Models\OrderStatus::where('status',5)->with('order')->get();
         foreach ($orders as $key => $order) {
                  $branchData = \Modules\Branches\Models\Branch::where('id',$order['order']->branch_id)->first();
                  $delevary = \Modules\DeliveryLocation\Models\DeliveryLocation::where([['branch_id','!=',$order['order']->branch_id],['city_id',$branchData->city_id],['country_id',$branchData->country_id]])->get();
                 if(count($delevary) > 0)
                 {
                    $other_branch = [];
                     foreach ($delevary as $key => $value) {
                        $other_branch = $value->branch_id;
                     }

                     $number_of_pending_roduct_in_main_branch  = DB::table('orders')
                          ->join('order_status', 'orders.id', '=', 'order_status.order_id')
                          ->select('orders.*', 'order_status.status')
                          ->where('orders.branch_id',$order['order']->branch_id)
                          ->where('order_status.status',5)
                          ->count();
                          $number_of_pending_roduct_in_other_branch  = DB::table('orders')
                          ->join('order_status', 'orders.id', '=', 'order_status.order_id')
                          ->select('orders.*', 'order_status.status')
                          ->where('orders.branch_id',$other_branch)
                          ->where('order_status.status',5)
                          ->count();
                          if(($number_of_pending_roduct_in_main_branch - $number_of_pending_roduct_in_other_branch) > 5) 
                            {
                               $order = \Modules\Orders\Models\Order::where('id',$order['order']->id)->first();
                               $order->branch_id = $other_branch;
                               $order->update();
                            }


                 }

       }
   }


}
