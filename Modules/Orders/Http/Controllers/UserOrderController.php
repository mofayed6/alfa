<?php

namespace Modules\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Orders\Models\Order;
use Modules\Orders\Models\OrderProduct;
use Carbon\Carbon;
use Modules\Orders\Models\OrderStatus;

class UserOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('orders::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('orders::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }


    /**
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $order = Order::with('products')->findORfail($id);
        $beforeDiscount = Order::productBeforeDiscountTotal($id);
        $afterDiscount = Order::productAfterDiscountTotal($id);
        $products  = Order::productData($id)->toArray();
        $date = Order::CheckStatusDate($id);
        $status  = Order::CheckStatus($id);

        return view('front.showSite',compact('order','date','beforeDiscount','afterDiscount','products','status'));
    }


    public function showAllOrder($all =null)
    {
        if($all == null) {
            $orders = Order::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(3);

            return view('front.trackOrder', compact('orders'));

        }elseif($all == 'more') {

            $ordersmore = Order::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(6);
            $view = view("front.moreOrder", compact('ordersmore'))->render();
            return response()->json(['html' => $view]);
        }elseif($all = 'last'){
            $ordersLast= Order::where("created_at",">", Carbon::now()->subMonths(6))->get();
            $view = view("front.lastorder", compact('ordersLast'))->render();
            return response()->json(['html' => $view]);
        }else{

            $ordersNew = Order::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->get();

            $view = view("front.file",compact('ordersNew'))->render();

            return response()->json(['html'=>$view]);
        }

    }



    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('orders::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
