<?php

namespace Modules\Orders\Models;

use App\Http\Traits\TranslatableTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Coupons\Models\Coupon;
use Modules\Products\Models\Product;
use Session;

class Order extends Model
{
   //use TranslatableTrait;
    protected $fillable = ['branch_id', 'coupon_id', 'user_id', 'tex_percentage'];
    protected $hidden=['priceAfterDiscount' ];

    public function products()
    {
        return $this->belongsToMany(Product::class,'order_products','order_id','product_id');
    }



    public function getPriceAfterDiscountAttribute()
    {
        return ($this->price - $this->discount_price);
    }



    public function orderStatus()
    {
        return $this->hasMany(OrderStatus::class,'order_id','id');
    }

    public static function productBeforeDiscountTotal($id)
    {
          $products = OrderProduct::where('order_id',$id)->get();
          $productPrice =[];
          $tax = [];
          $productPriceDiscount =[];
           foreach ($products as $product){

                   $productPriceDiscount[] =  $product->discount_price ;

                   $productPrice[] =  $product->price ;

           }


          $sumDiscpunt =  array_sum($productPriceDiscount) ;
          $sumPrice =  array_sum($productPrice);
          if($sumDiscpunt < $sumPrice){
              return $sumDiscpunt;
          }else{
              return  $sumPrice;
          }
    }

    public static function totalPrice($id)
    {
        $subTotal = self::productBeforeDiscountTotal($id);
        $order = Order::find($id);
        $tax = $subTotal  * $order->tex_percentage / 100;
        $cupon = Coupon::where('id',$order->coupon_id)->first();
        $totalBeforCoupon = $subTotal + $tax ;

        if($cupon->amount_type === 'percentage' ){
          $cuponNumber =   $totalBeforCoupon  * $cupon->minimum_amount / 100 ;
        }else{
            $cuponNumber =   $totalBeforCoupon  * $cupon->minimum_amount / 100 ;
        }
        return $cuponNumber;
    }


   public static function productAfterDiscountTotal($id)
    {
           $products = OrderProduct::where('order_id',$id)->get();
           return  $products->sum('price') - $products->sum('discount_price') + 17;
    }


    public static function productData($id)
    {
        $productss = OrderProduct::where('order_id',$id)->with('products')->get();
        return $productss;
    }


    public static function CheckStatus($id)
    {
        $status = OrderStatus::where([['order_id',$id]])->first();
        if($status){
            return $status;
        }
        return '';

    }

    public static function CheckStatusDate($id)
    {
        $status = OrderStatus::where([['order_id',$id]])->first();
        if ($status) {
            $date = new Carbon($status->updated_at);
            $carbon = $date->toFormattedDateString();
            return $carbon;

        }
    }






    public static function lastOrderAdd()
    {
        $last = Order::latest('id')->first();
        return $last;
    }

    public function orderProduct($order_id){
       $orderProduct = OrderProduct::where('order_id',$order_id)->pluck('order_id','product_id')->toArray();
       return $orderProduct;
    }

    public static function product($order_id)
    {
        $product = Product::whereIn('id',array_keys((new self)->orderProduct($order_id)))->get()->toArray();

       return $product;

    }

    public static function setSession()
    {
        $value = Order::count();
        $session =  session('orderIdCount',$value);
       // dd($session);
        return $session;
    }

    public function getNameAttribute()
    {
        return $this->name;
    }




}
