<?php

namespace Modules\Orders\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Products\Models\Product;

class OrderProduct extends Model
{
    //protected $fillable = ['product_id', 'order_id', 'price', 'discount_price', 'quantity', 'discount_from', 'discount_to'];
    protected $table = 'order_products';
   // use TranslatableTrait;

    public function products()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
}
