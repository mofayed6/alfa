<?php

namespace Modules\Orders\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    //use TranslatableTrait;
    protected $fillable = ['status'];
    protected $table = 'order_status';

    public static function status()
    {
        $status = ['1'=>'processing','2'=>'delivering','3'=>'delivered','4'=>'canceled','5'=>'pending'];
        return $status;
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id','id');
    }


    public static function statusName($value)
    {
        switch ($value) {
            case "1":
                return "Processing";
                break;
            case "2":
                echo "Delivering";
                break;
            case "3":
                echo "Delivered";
                break;
            case "4":
                echo "Canceled";
                break;
            case "5":
                echo "Pending";
                break;
            default:
                echo "";
        }
    }
}
