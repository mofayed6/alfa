<?php


Route::middleware('admin:admin')->prefix('admin/orders')->group(function() {

//order
    Route::get('/datatable', 'OrdersController@datatable');
    Route::get('/create', 'OrdersController@create');
    Route::get('/{order}/edit', 'OrdersController@edit');
    Route::patch('/{order}/updatee', 'OrdersController@update')->name('orders.last.update');

    Route::get('/', 'OrdersController@index');
    Route::post('/orders','OrdersController@store')->name('orders.store');
    Route::get('/{id}/delete','OrdersController@destroy');
    Route::get('/count','OrdersController@countOrderDatabase');
    Route::get('/change','OrdersController@ChangeDelevary');
    Route::get('/changeProduct/{id}','OrdersController@productPrice')->name('price');


//product order
    Route::get('/{parent}', 'ProductOrderController@indexOrder');
    Route::get('/create/{parent}', 'ProductOrderController@createProduct');
    Route::post('/{parent}','ProductOrderController@storeProductOrder')->name('orders.product.store');
    Route::get('/products/datatable/{order?}', 'ProductOrderController@datatableProduct');
    Route::get('/{id}/{proId}/order-product', 'ProductOrderController@editOrderProduct')->name('orders.product.edit');
    Route::patch('/{order}/update', 'ProductOrderController@updateOrderProduct')->name('orders.product.update');
    Route::get('/{id}/{proId}/delete','ProductOrderController@destroyOrderProduct');


});


//front show order
Route::get('orders/{id}','UserOrderController@show');
Route::get('/all-orders/{all?}','UserOrderController@showAllOrder');




