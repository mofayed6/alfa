@extends('admin.layouts.master')
@section('page-title','Edit Order')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/orders')}}">@yield('page-title')</a></li>
    <li class="breadcrumb-item active">Edit Order</li>
@endsection
@section('content')
    {!! Form::model($order,['url'=>['admin/orders/'.$order->id.'/updatee'],'method'=>'PATCH','files'=>true]) !!}

    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Edit Order </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('admin.pratical.message')

                    <div class="form-group">
                        <label>Branch</label>
                        {{Form::select('branch_id',$branchs,null,['class'=>'form-control'])}}
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<label>Product</label>--}}
                    {{--{{Form::select('product_id',$poducts,null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label>Coupones</label>
                        {{Form::select('coupon_id',$coupones,null,['class'=>'form-control'])}}
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<label>Price</label>--}}
                    {{--{{Form::number('price',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label>User</label>
                        <select class="form-control" name="user_id">
                            @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<label>Discount Price</label>--}}
                    {{--{{Form::number('discount_price',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label>Tax </label>
                        {{Form::number('tex_percentage',null,['class'=>'form-control'])}}
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<label>Quantity</label>--}}
                    {{--{{Form::number('quantity',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label>Status</label>
                        {{Form::select('status_id',$status,null,['class'=>'form-control'])}}
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>


                    </div>
                </div>


            </div>


        </div>




    </div>

    {{Form::close()}}

@endsection
@section('styles')
    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />

@endsection

@section('scripts')
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/piexif.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/sortable.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/plugins/purify.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/assets/vendor/bootstrap-fileinput/js/fileinput.min.js')}}" type="text/javascript"></script>

@endsection
