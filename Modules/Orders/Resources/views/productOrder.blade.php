@extends('admin.layouts.master')
@section('page-title','Orders')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/orders')}}">@yield('page-title')</a></li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Order Product list</h2>
                </div>
                <div class="col-md-6">
                    <a href="{{url('/admin/orders/create/'.$id)}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product</th>
                        <th>Option</th>
                    </tr>
                    </thead>
                    {{--<tbody>--}}
                    {{--@foreach($porducts as $product)--}}
                    {{--<tr role="row" >--}}
                        {{--<td >{{$product['id']}}</td>--}}
                        {{--<td>{{$product['en']['name']}}</td>--}}
                        {{--<td>--}}
                            {{--<a href="{{ url('admin/orders/'.$id.'/'.$product['id'].'/order-product') }}" class="btn btn-warning">Edit</a>--}}
                            {{--<a href="{{ url('admin/orders/'.$id.'/'.$product['id'].'/delete') }}" class="btn btn-danger">Delete</a>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    {{--@endforeach--}}
                    {{--</tbody>--}}
                </table>
            </div>
        </div>
    </div>
@endsection




@section('scripts')
    <script>

        $(document).ready(function() {
            $('.datatable').DataTable( {
                "processing": true,
                "serverSide": true,
                ajax: '{!! url('admin/orders/products/datatable/'.$id) !!}',
                "columns": [
                    { data: 'id', name: 'id' },
                    { data: 'product', name: 'product' },
                    { data: 'options', name: 'options' },
                ]
            } );

        } );
    </script>
@stop


