@extends('admin.layouts.master')
@section('page-title','Orders')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Order list</h2>
                </div>
                <div class="col-md-6">
                    <a href="{{url('/admin/orders/create/')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Branch</th>
                        <th>Option</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection



@section('scripts')
    <script>



            $('.datatable').DataTable( {
                "processing": true,
                "serverSide": true,
                ajax: '{!! url('admin/orders/datatable/') !!}',
                "columns": [
                    { data: 'id', name: 'id' },
                    { data: 'branch_id', name: 'branch_id' },
                    { data: 'options', name: 'options' },
                ]
            } );

            var lastCount = '{!! Session::get('orderIdCount') !!}';

           setInterval(function() {
               $.ajax({
                   url: "{!! url('admin/orders/count/') !!}",
                   success: function(result){
                       if(result >= lastCount)
                       {
                           $("datatable").load(location.href + " .datatable");
                       }
                   }});

              },  1000);



            setInterval(function() {
               $.ajax({
                   url: "{!! url('admin/orders/change/') !!}",
                   success: function(result){
                       if(result >= lastCount)
                       {
                           $("datatable").load(location.href + " .datatable");
                       }
                   }});

              },  10000);



    </script>
@stop
