@extends('admin.layouts.master')
@section('page-title','Create Order')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/orders')}}">Orders</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    {{Form::open(['route' => ['orders.product.store',$id]])}}

    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Add New Order </h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    @include('admin.pratical.message')



                    <div class="form-group">
                    <label>Product</label>
                    {{Form::select('product_id',$poducts,null,['class'=>'form-control'])}}
                    </div>


                    <div class="form-group">
                    <label>Price</label>
                    {{Form::number('price',null,['class'=>'form-control'])}}
                    </div>


                    <div class="form-group">
                    <label>Discount Price</label>
                    {{Form::number('discount_price',null,['class'=>'form-control'])}}
                    </div>


                    <div class="form-group">
                    <label>Quantity</label>
                    {{Form::number('quantity',null,['class'=>'form-control'])}}
                    </div>


                    <div class="form-group">
                        <label>Coupon Date</label>
                        {{Form::text('date',null,['class'=>'form-control','readonly'])}}
                    </div>



                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Save</button>


                    </div>
                </div>


            </div>


        </div>




    </div>


    {{Form::close()}}

@endsection
@section('styles')
    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}">

@endsection


@section('scripts')
    <script src="{{ asset('assets/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(document).ready(function () {

            $('input[name="date"]').daterangepicker({
                minDate: new Date(),
                timePicker: true,
                locale: {
                    format: 'Y-M-DD HH:mm:ss'
                }
            });

        });



    </script>
@endsection

