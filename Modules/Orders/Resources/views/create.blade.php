@extends('admin.layouts.master')
@section('page-title','Create Order')
@section('style')

@stop
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/orders')}}">Orders</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')

    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}

            {{--<div class="card">--}}
                {{--<div class="header">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<h2>Add New Order </h2>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="body">--}}
                    {{--@include('admin.pratical.message')--}}

                    {{--<div class="form-group">--}}
                        {{--<label>Branch</label>--}}
                        {{--{{Form::select('branch_id',$branchs,null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label>Product</label>--}}
                        {{--{{Form::select('product_id',$poducts,null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label>Coupones</label>--}}
                        {{--{{Form::select('coupon_id',$coupones,null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label>Price</label>--}}
                        {{--{{Form::number('price',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label>User</label>--}}
                        {{--<select class="form-control" name="user_id">--}}
                            {{--@foreach($users as $user)--}}
                                {{--<option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label>Discount Price</label>--}}
                        {{--{{Form::number('discount_price',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label>Tax </label>--}}
                        {{--{{Form::number('tex_percentage',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label>Status</label>--}}
                        {{--{{Form::select('status_id',$status,null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label>Quantity</label>--}}
                        {{--{{Form::number('quantity',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}



                    {{--<div class="form-group">--}}
                        {{--<button type="submit" class="btn btn-success">Save</button>--}}


                    {{--</div>--}}
                {{--</div>--}}


            {{--</div>--}}


        {{--</div>--}}


        {{--<div class="col-md-3">--}}
            {{--<div class="card">--}}
                {{--<div class="header">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<h2>Main Features</h2>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="body">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Branch</label>--}}
                        {{--{{Form::select('branch_id',$branchs,null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Coupones</label>--}}
                        {{--{{Form::select('coupon_id',$coupones,null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<label>User</label>--}}
                        {{--<select class="form-control" name="user_id">--}}
                            {{--@foreach($users as $user)--}}
                                {{--<option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}


                    {{--<div class="form-group">--}}
                        {{--<label>Tax </label>--}}
                        {{--{{Form::number('tex_percentage',null,['class'=>'form-control'])}}--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}


        {{--</div>--}}



    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="header">
                    <h2>Order Details</h2>

                </div>
                <div class="body">
                    {{--<form id="wizard_with_validation" method="POST">--}}
                    {{Form::open(['route'=>'orders.store','files'=>true , 'id'=>'wizard_with_validation'])}}

                    <h3>Main Information</h3>
                        <fieldset>
                            <div class="form-group form-float">
                                {{--<input type="text" class="form-control" placeholder="Username *" name="username" required>--}}
                                <label>Branch</label>
                                {{Form::select('branch_id',$branchs,null,['class'=>'form-control disease','required'=>'required','placeholder'=>'choose Branch ..'])}}
                            </div>
                            <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                                <label>Coupones</label>
                                {{Form::select('coupon_id',$coupones,null,['class'=>'form-control disease','placeholder'=>'choose coupons ..'])}}
                            </div>
                            <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Confirm Password *" name="confirm" required>--}}
                                <label>User</label>
                                <select class="form-control disease" name="user_id" id="user" placeholder="Choose User .."  required>
                                 <option value="new">create new user</option>
                                @foreach($users as $user)
                                <option value="{{$user->id}}">{{$user->first_name .' '.$user->last_name}}</option>
                                @endforeach
                                </select>

                            </div>

                            <div class="clearfix"></div>
                            <a href="{{ url('admin/user/create') }}" target="_blank" id="button" style="display:none;background-color: #007bff;color: white;padding: 14px 25px;text-align: center;text-decoration: none;"><i class="fa fa-plus-square"></i>create new user</a>

                            <div class="clearfix"></div>
                            <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                                <label>Tax </label>
                                {{Form::number('tex_percentage',null,['class'=>'form-control','placeholder'=>'write tax'])}}
                            </div>

                        </fieldset>
                        <h3>Product Information</h3>
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                               <div class="form-group form-float">
                                {{--<input type="text" class="form-control" placeholder="Username *" name="username" required>--}}
                                <label>Product</label><br>
                                {{Form::select('product_id',$poducts,null,['class'=>'form-control disease','id'=>'product','style'=>'width:100%','required'=>'required','placeholder'=>'choose Product ..'])}}
                            </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Password *" name="password" id="password" required>--}}
                                <label>Price</label>
                                {{Form::number('price',null,['class'=>'form-control','id'=>'price','disabled','style'=>'width:33%',])}}
                            </div>
                            </div>
                            <div class="form-group form-float">
                                {{--<input type="password" class="form-control" placeholder="Confirm Password *" name="confirm" required>--}}
                                <label>Discount Price</label>
                                {{Form::number('discount_price',null,['class'=>'form-control','required'=>'required','style'=>'width:33%',])}}
                            </div>
                            <div class="form-group form-float">
                                <label>Quantity</label>
                                {{Form::number('quantity',null,['class'=>'form-control','required'=>'required','style'=>'width:33%',])}}
                            </div>
                            <div class="form-group form-float">
                                <label>Coupon Date</label>
                                {{Form::text('date',null,['class'=>'form-control','readonly'])}}
                            </div>
                        </div>
                    </fieldset>

                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>


@endsection
@section('styles')
    <link href="{{asset('/assets/vendor/bootstrap-fileinput/css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}">


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
@endsection


@section('scripts')




    <script src="{{ asset('assets/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js')}}"></script>

    <script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script> <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/vendor/jquery-steps/jquery.steps.js')}}"></script> <!-- JQuery Steps Plugin Js -->

    <script src="{{ asset('assets/bundles/mainscripts.bundle.js')}}"></script>
    <script src="{{ asset('assets/js/pages/forms/form-wizard.js')}}"></script>




    <script>
        $(document).ready(function () {

            $('input[name="date"]').daterangepicker({
                minDate: new Date(),
                timePicker: true,
                locale: {
                    format: 'Y-M-DD HH:mm:ss'
                }
            });




        });



    </script>


    <script>
        $(document).ready(function() {
            // $('.disease').select2();
            $('.disease').select2();


            $( "#product" ).change(function() {
                var id =$(this).val();
                $.ajax({
                    method: "GET",
                    url: "{!! url('admin/orders/changeProduct') !!}" +'/'+id,
                }).done(
                    function(data)
                    {
                        $('#price').val(data);
                    }

                );
            });


            $( "#user" ).change(function() {
                var value =$(this).val();
                if(value === 'new')
                {
                    $("#button").show();
                }else{
                    $("#button").hide();
                }

            });

        });


    </script>
@endsection

