<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('selection_type',['single','multiple']);
            $table->string('default_value',60);
            $table->string('template',60);
            $table->boolean('is_required');
            $table->timestamps();
        });

        Schema::table('attributes', function (Blueprint $table) {
            //set integers length
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN id INT(3) AUTO_INCREMENT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
