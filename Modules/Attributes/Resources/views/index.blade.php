@extends('admin.layouts.master')
@section('page-title','Attributes')
@section('breadcrumb')
    <li class="breadcrumb-item active">Attributes</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <a href="{{url('admin/attributes/create/')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square"></i> <span>Add New</span></a>
            <h2>Products Attributes</h2>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <div class="table-responsive">
                <table class="table table-hover m-b-0 c_list datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Template</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('admin/attributes/datatable/') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'template', name: 'template' },
                    { data: 'actions', name: 'actions' },
                ],
            });
        });

        {{--function approve($id){--}}
            {{--$.get('{{url('dashboard/countries/flag/')}}/'+$id,function (response) {--}}
                {{--if(response.state == 'deapproved'){--}}
                    {{--$('a[data-id="'+$id+'"]').removeClass('btn-danger').addClass('btn-success').text('approve');--}}
                {{--}else{--}}
                    {{--$('a[data-id="'+$id+'"]').removeClass('btn-success').addClass('btn-danger').text('Deapprove');--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}
    </script>
@stop
