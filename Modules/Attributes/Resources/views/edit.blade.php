@extends('admin.layouts.master')
@section('page-title','Attributes')
@section('breadcrumb')
    <li class="breadcrumb-item active">Attributes</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <h2>Products Attributes</h2>
        </div>
        <div class="body">
            @include('admin.pratical.message')
            <ul class="nav nav-tabs">
                @foreach(locales() as $k=>$local)
                    <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                @endforeach
            </ul>
            {{Form::model($attr,['method'=>'PATCH','action'=>['\Modules\Attributes\Http\Controllers\AttributesController@update',$attr->id]])}}
            <div class="tab-content">
                @foreach(locales() as $k=>$local)
                    <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                        <div class="form-group">
                            <label>Name {{ $local->code}} </label>
                            {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
                        </div>
                    </div>
                @endforeach

                <div class="panel-body">
                    <div class="form-group">
                        <label for="input-default-value">Default Value</label>
                        {{Form::text('default_value',null,['id'=>'default_value',"class"=>"form-control"])}}
                    </div>
                    <div class="form-group">
                        <label for="option-template">Template</label>
                        {{Form::select('template',['input_text'=>'Text Input','dropdown'=>'Drop Down'],null,['class'=>'form-control chosen-select chosen-rtl','id'=>'option-template'])}}
                    </div>

                    <div class="form-group">
                        <label class="fancy-checkbox">
                            {{Form::checkbox('is_required',1,null)}}
                            <span>Required ?</span>
                        </label>

                    </div>

                    <div class="form-group if-dropdown">
                        <label for="selection_type">Type</label>
                        {{Form::select('selection_type',['single'=>'Single','multiple'=>'Multiple'],null,['class'=>'form-control chosen-select chosen-rtl','id'=>'selection_type'])}}
                    </div>
                </div>
                <div class="panel-body if-dropdown">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="input-option-slug">Slug</label>
                                {{Form::text('',null,['class'=>'form-control','id'=>'input-option-slug'])}}
                            </div>
                        </div>
                        @foreach(locales() as $locale)
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="input-option-ar">Option {{$locale->code}}</label>
                                    {{Form::text('',null,['class'=>'form-control','id'=>'input-option-'.$locale->code])}}
                                </div>
                            </div>
                        @endforeach
                        <div class="col-md-2">
                            <div class="if-new">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-labeled add-attribute-option">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="if-edit" style="display: none;">
                                <button type="button" class="btn btn-success btn-labeled" id="save-option">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-danger btn-labeled" id="discard-option">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <fieldset>
                                <legend>Options</legend>
                                <div class="dd" id="nestable">
                                    <ol class="dd-list"></ol>
                                </div>
                            </fieldset>

                        </div>
                    </div>
                </div>
                {{Form::hidden('options',$options)}}
                <button type="submit" class="btn btn-success">Save</button>
            </div>
            {{Form::close()}}
        </div>
    </div>
@endsection
@section('styles')
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery/jquery-sortable.css')}}">
    <script src="{{ asset('assets/vendor/jquery/jquery-sortable.js') }}"></script>
    <style>
    </style>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('assets/vendor/jquery/jquery-sortable.js') }}"></script>

    <script>

        $(document).ready(function () {
            $('.dd').nestable();

            function getOptionsFromInput()
            {
                oldOptions = $('input[name="options"]').val();
                if(oldOptions.trim() === '')
                    return false;
                jsonData = JSON.parse(oldOptions);
                $(".dd-list").empty();
                $.each(jsonData,function (key,res) {
                    $(".dd-list").append(
                        '<li class="dd-item" data-slug="'+res.slug+'" @foreach(locales() as $locale) data-option-{{$locale->code}}="'+ res.option{{ucfirst($locale->code)}} +'"  @endforeach  >' +
                        '<div class="dd-handle"> @foreach(locales() as $k=> $locale) {{($k==0)?"":"-"}} '+res.option{{ucfirst($locale->code)}}+'@endforeach</div>' +
                        '<a class="btn dd-edit-item"><i class="fa fa-edit"></i></a>' +
                        '<a class="btn dd-delete-item"><i class="fa fa-trash"></i></a>' +
                        '</li>'
                    );
                });
            }
            choose_template();

            $('#option-template').change(function () {
                choose_template();
            });

            function choose_template() {
                var temp = $('#option-template').val();

                if (temp == 'dropdown') {
                    $('.if-dropdown').show();
                    $("#default_value").replaceWith('{{Form::select('default_value',[],null,['class'=>'form-control','id'=>'default_value'])}}');
                    getOptionsFromInput();
                    generateDefaultSelectFromJson();
                } else {
                    $('.if-dropdown').hide();
                    $("#default_value").replaceWith('{{Form::text('default_value',null,["class"=>"form-control",'id'=>'default_value'])}}');
                }
            }

            $('.add-attribute-option').on('click',function () {
                if($('#input-option-slug').val() == ''@foreach(locales() as $k=>$locale) || $('#input-option-{{$locale->code}}').val() == ''@endforeach){
                    return alert('all option inputs must be filled');

                }

                $slug  = $('#input-option-slug').val();

                $(".dd-list").append(
                    '<li class="dd-item" data-slug="'+$slug+'" @foreach(locales() as $locale) data-option-{{$locale->code}}="'+ $('#input-option-{{$locale->code}}').val() +'"  @endforeach  >' +
                    '<div class="dd-handle"> @foreach(locales() as $k=> $locale) {{($k==0)?"":"-"}} '+$('#input-option-{{$locale->code}}').val()+'@endforeach</div>' +
                    '<a class="btn btn-icon-only dd-edit-item"><i class="fa fa-edit"></i></a>' +
                    '<a class="btn btn-icon-only dd-delete-item"><i class="fa fa-trash"></i></a>' +
                    '</li>'
                );
                $('#input-option-slug').val('');
                @foreach(locales() as $locale)  $('#input-option-{{$locale->code}}').val(''); @endforeach
                    newSerializedJSON = JSON.stringify($('.dd').nestable('serialize'));
                $('input[name="options"]').val(newSerializedJSON);
                generateDefaultSelectFromJson()
            });


            $('.dd').on('change', function() {
                jsonData = $('.dd').nestable('serialize');
                newSerializedJSON = JSON.stringify(jsonData);
                generateDefaultSelectFromJson(newSerializedJSON);
                $('input[name="options"]').val(newSerializedJSON);
            });
            $(document).on('click','.dd-delete-item',function () {
                conf = confirm('are you sure ?');
                if(!conf){
                    return false;
                }
                $(this).parent().remove();
                generateDefaultSelectFromJson();
            });

            function generateDefaultSelectFromJson()
            {
                jsonData = $('.dd').nestable('serialize');
                var $options ;
                $('#default_value').empty();
                $.each(jsonData,function (key,res) {
                    $('#default_value').append($('<option>').val(res.slug).text(res.optionEn+' - '+res.optionAr).attr('selected',(res.slug == '{{$attr->default_value}}') ) );
                });

            }
            $(document).on('click','.dd-edit-item',function () {
                $slug = $(this).parent().attr('data-slug');
                $('#input-option-slug').val($slug);
                @foreach(locales() as $locale)
                    $option{{ucfirst($locale->code)}} = $(this).parent().attr('data-option-{{$locale->code}}');
                $('#input-option-{{$locale->code}}').val($option{{ucfirst($locale->code)}});
                @endforeach
                $('.if-new').hide();
                $('.if-edit').show();
                $('#save-option').attr('data-slug',$slug);
            });

            $('#discard-option').on('click',function () {
                $('#input-option-slug').val('');
                @foreach(locales() as $locale)
                $('#input-option-{{$locale->code}}').val('');
                @endforeach
                $('.if-new').show();
                $('.if-edit').hide();
            });

            $('#save-option').on('click',function () {
                $slug = $(this).attr('data-slug');
                @foreach(locales() as $locale)
                    $option{{ucfirst($locale->code)}} = $('#input-option-{{$locale->code}}').val();
                @endforeach
                    jsonData = $('.dd').nestable('serialize');
                withoutCurrnt = $.grep(jsonData, function( n, i ) {
                    return n.slug !== $slug;
                });
                withoutCurrnt.push({
                    slug:$slug,
                    @foreach(locales() as $locale)
                    option{{ucfirst($locale->code)}} :  $option{{ucfirst($locale->code)}},
                    @endforeach
                });
                newSerializedJSON = JSON.stringify(withoutCurrnt);
                $('input[name="options"]').val(newSerializedJSON);
                getOptionsFromInput();
                generateDefaultSelectFromJson();
                $('#input-option-slug').val('');
                @foreach(locales() as $locale)
                $('#input-option-{{$locale->code}}').val('');
                @endforeach
                $('.if-new').show();
                $('.if-edit').hide();
            });


        });

    </script>
@stop
