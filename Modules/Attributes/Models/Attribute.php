<?php

namespace Modules\Attributes\Models;

use App\Http\Traits\TranslatableTrait;
use App\Http\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Categories\Models\Category;
use Modules\Products\Models\Product;

class Attribute extends Model
{
    use TranslatableTrait;

    protected $table        = 'attributes';
    protected $fillable     = ['selection_type','default_value','template','is_required'];
    protected $translatable = ['name', 'options'];
    protected $casts        = ['options' => 'json'];

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_attributes','attribute_id','category_id');
    }


    public function products()
    {
        return $this->belongsToMany(Product::class,'attributes_product','attribute_id','product_id');
    }

    public function setDefaultValueAttribute($default_value)
    {
        if($default_value)
            return $default_value;
        return "";
    }


    public static function getAttributesByCategoriesArray(array $categories_array)
    {
        $attributesID = \DB::table('category_attributes')->whereIn('category_id',$categories_array)->pluck('attribute_id');
        $attributes = Attribute::find($attributesID);
        return $attributes;
    }



    public static function getParentCategoriesAttributes($parentId)
    {
        $parentCategories = Category::getParentsFlat($parentId);
        return Attribute::getAttributesByCategoriesArray(array_keys($parentCategories));
    }

}
