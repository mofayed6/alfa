<?php

namespace Modules\Attributes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Attributes\Models\Attribute;

class AttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('attributes::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('attributes::create');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'ar.name'=>'required',
            'template'=>'required'
        ]);
        if($request->template =='dropdown'){
            $this->validate($request,[
                'options'=>'required',
            ]);
        }
        if($request->filled('options'))
        {
            foreach (locales() as $locale) {
                $option = array_merge($request->{$locale->code},$this->formattingOptionsForDB(json_decode($request->options, true))[$locale->code]);
                $request->merge([$locale->code => $option]);
            }
        }
        $attributes = new Attribute();
        $attributes->selection_type = $request->selection_type;
        $attributes->default_value  = $request->default_value;
        $attributes->template       = $request->template;
        $attributes->is_required    = ($request->filled('is_required'))?$request->is_required:0;
        $attributes->save();
        $attributes->translateAttributes($request->all());
        return redirect('admin/attributes')->with(['success'=>'Attribute Add Successfully']);
    }

    public function formattingOptionsForDB($options)
    {
        $values = [];
        $results = [];
        foreach (locales() as $locale){
            foreach($options as $k => $option){
                $values[$locale->code][$k]['slug'] = $option['slug'];
                $values[$locale->code][$k]['name'] = $option['option'.ucfirst($locale->code)];
            }
        }
        foreach (locales() as $locale) {
            $results[$locale->code]['options'] = json_encode($values[$locale->code]);
        }
        return $results;
    }

    public function formattingOptionsForView($attr)
    {
        $results = [];
        $options = [];
        foreach (locales() as $locale){
           $options[$locale->code] = $attr->{$locale->code}['options'];
            foreach ($options[$locale->code] as $k=>$option) {
                $results[$k]['slug'] = $options[$locale->code][$k]->slug;
                $results[$k]['option'.ucfirst($locale->code)] = $attr->{$locale->code}['options'][$k]->name;
            }
        }
        return json_encode($results);
    }


    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('attributes::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Attribute $attr)
    {
        $options = "";
        if($attr->template =='dropdown') {
            $options = $this->formattingOptionsForView($attr);
        }
        return view('attributes::edit',compact('attr','options'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Attribute $attr,Request $request)
    {
        $this->validate($request,[
            'en.name'=>'required',
            'ar.name'=>'required',
            'template'=>'required'
        ]);
        if($request->template =='dropdown'){
            $this->validate($request,[
                'options'=>'required',
            ]);
        }
        foreach (locales() as $locale) {
            $option = array_merge($request->{$locale->code},$this->formattingOptionsForDB(json_decode($request->options, true))[$locale->code]);
            $request->merge([$locale->code => $option]);
        }
        $attr->selection_type = $request->selection_type;
        $attr->default_value  = $request->default_value;
        $attr->template       = $request->template;
        $attr->is_required    = ($request->filled('is_required'))?$request->is_required:0;
        $attr->save();
        $attr->deleteTranslations();
        $attr->translateAttributes($request->all());
        return redirect('admin/attributes')->with(['success'=>'Attribute Add Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy( $id )
    {
        $attr = Attribute::find($id);
        $attr->delete();
        $attr->deleteTranslations();
        return redirect('admin/attributes')->with(['success'=>'Attribute Deleted Successfully']);
    }

    public function datatable()
    {
        $categories = Attribute::get();
        return \DataTables::of($categories)
            ->editColumn('name',function ($model){
                return $model->name;
            })->editColumn('actions',function ($model){
                return "<a href='".url('admin/attributes/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                        <a href='".url('admin/attributes/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";
            })
            ->make(true);
    }

    public function list(Request $request,$parent = null)
    {
        $attributes = Attribute::query();
        if($parent != null)
            $attributes = Attribute::query();

        if($request->filled('q')){
            $attributes->whereHas('translations',function ($query) use($request){
                $query->where('attribute','name')->where('value','like','%'.$request->q.'%');
            });
        }
        $parent_attributes = Attribute::getParentCategoriesAttributes($parent);
        $attributes = $attributes->whereNotIn('id',$parent_attributes->pluck('id'))->get();
        return \response()->json($attributes);

    }
}
