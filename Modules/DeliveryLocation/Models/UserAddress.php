<?php

namespace Modules\DeliveryLocation\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Locations\Models\Location;
use Modules\User\Models\User;

class UserAddress extends Model
{
   
   protected $table = 'user_addresses';

   public function User()
   {
      return $this->belongsTo(User::class);
   }

}
