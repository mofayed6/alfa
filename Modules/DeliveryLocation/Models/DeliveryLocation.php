<?php

namespace Modules\DeliveryLocation\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Modules\Branches\Models\Branch;
use Modules\Locations\Models\Location;
use Modules\User\Models\User;

class DeliveryLocation extends Model
{
    use TranslatableTrait;

    protected $fillable = [];
    protected $translatable = ['name'];
    protected $appends      = ['name'];

    public function children()
    {
      return  $this->belongsTo(DeliveryLocation::class,'parent_id');
    }

    public function childrenRecursive()
    {
      return  $this->children()->with('childrenRecursive');
    }



    public function parent()
    {
        $this->depth++;
        return $this->belongsTo(DeliveryLocation::class, 'parent_id');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }


    public static function getParentsFlat($parent = null)
    {
        $items = DeliveryLocation::with('parentRecursive')->where('id', $parent)->first();
        if(!$items){
            return [];
        }
        $items = $items->toArray();
        $items = (new self)->recursiveParentToFlat($items,'parent_recursive',0);
        $items = array_reverse($items,true);
        unset($items[$parent]);
        //array_pop($items);

        return $items;
    }

    protected function recursiveParentToFlat($item,$recursive_key='children_recursive',$i)
    {
        if(count($item) == 0 )
            $results = [];
        $results[$item['id']] = $item['name'];
        if (is_array($item[$recursive_key]) && count($item[$recursive_key]) > 0) {
            $i++;
            $results = $results + $this->recursiveParentToFlat($item[$recursive_key],$recursive_key,$i);
        }
        return $results;
    }



    public static function getChildrenFlat($parent = null, $maxDepth=3)
    {
        $items = self::with('childrenRecursive')->where('parent_id', $parent)->get()->toArray();
        $items = (new self)->recursiveChildrenToFlat($items,'-',1,$maxDepth);
        return $items;
    }

    protected function recursiveChildrenToFlat($array, $dashes = "-",$depth=1,$maxDepth=4,$recursive_key='children_recursive')
    {
        $results = [];
        if($depth<=$maxDepth){
            foreach ($array as $k => $item) {
                $results[$item['id']] = $dashes . ' ' . $item['name'];
                if (is_array($item[$recursive_key]) && count($item[$recursive_key]) > 0) {
                    $results = $results + $this->recursiveChildrenToFlat($item[$recursive_key], '-' . $dashes,++$depth,$maxDepth);
                }
            }
        }
        return $results;
    }


    public static function getAll()
    {
        $results = [];
        $countries = Location::where('parent_id',null)->get();
        foreach ($countries as $key=>$country)
        {
            $results[$key]['id'] = $country->id;
            $results[$key]['name'] = $country->name;

            foreach ($country->children as $x=>$city)
            {
                $results[$key]['cities'][$x]['id'] = $city->id;
                $results[$key]['cities'][$x]['name'] = $city->name;

                foreach ($city->children as $z=>$district) {
                    $results[$key]['cities'][$x]['districts'][$z]['id'] = $district->id;
                    $results[$key]['cities'][$x]['districts'][$z]['name'] = $district->name;
                }
            }

        }

        return $results;
    }


    public function getNameAttribute()
    {
        return $this->en['name'];
    }



    public static function CalcolateDistance($branch,$user)
    {
        $branch = Branch::find($branch);

        $user = UserAddress::where([['user_id',$user],['is_default',1]])->first();

       return $distance =  (new self)-> GetDrivingDistance($branch->latitude,$branch->longitude,$user->lat,$user->lng);

    }



    function GetDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=en&key=AIzaSyDvt4xYX0QycPedzqGKJ7_1sg6KH_iztDA";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        //dd($response_a);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

        return array('distance' => $dist, 'time' => $time);
    }


}
