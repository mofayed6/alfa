<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->unsignedInteger('parent_id');
            $table->timestamps();
        });

        Schema::table('delivery_locations', function (Blueprint $table) {
            //set integers length
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN id INT(4) AUTO_INCREMENT');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN country_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN city_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN district_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN branch_id INT(7)');
            DB::statement('ALTER TABLE ' . $table->getTable() . ' MODIFY COLUMN parent_id INT(7)');
            //foreign key constrains
            $table->foreign('country_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('district_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on($table->getTable())->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_locations');
    }
}
