<?php

namespace Modules\DeliveryLocation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\DeliveryLocation\Models\UserAddress;
class UserDeliveingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('deliverylocation::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('deliverylocation::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function storeAddress(Request $request)
    {
        $request->validate([
            'address'=>'required',
        ]);

        $address = new UserAddress();

        $address->user_id           = auth()->user()->id;
        $address->address           = $request->address;
        $address->lat               = $request->lat;
        $address->lng               = $request->lng;
        $address->is_default          = '0';

        $address->save();

        return redirect()->back();

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {

        return view('front.addressDelivary');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('deliverylocation::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
