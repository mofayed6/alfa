<?php

namespace Modules\DeliveryLocation\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Branches\Models\Branch;
use Modules\DeliveryLocation\Models\DeliveryLocation;
use Modules\DeliveryLocation\Models\PriceDelivary;

class DeliveryLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($parent = null)
    {
        if($parent)
            $category = DeliveryLocation::findOrFail($parent);
        return view('deliverylocation::index',compact('parent'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($parent=null)
    {
        $parentLocation = null;
        if($parent != null )
         $parentLocation = DeliveryLocation::findOrFail($parent);
         $branches = Branch::all()->pluck('name','id');
         $locations =  DeliveryLocation::getAll();
        return view('deliverylocation::create',compact('locations','branches','parentLocation','parent'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'en.name'=>'required',
            'ar.name'=>'required',
            'country_id'=>'required',
            'city_id'=>'required',
            'district_id'=>'required',
            'branch_id'=>'required',
        ]);

        $delivery = new DeliveryLocation();
        $delivery->country_id     = $request->country_id;
        $delivery->city_id        = $request->city_id;
        $delivery->district_id    = $request->district_id;
        $delivery->branch_id       = $request->branch_id;
        $delivery->parent_id        = $request->parent_id;

        $delivery->save();
        $delivery->translateAttributes($request->only(['ar','en']));



        $price =  PriceDelivary::find(1);
        $price->min          = $request->min;
        $price->max          = $request->max;
        $price->price        = $request->price;
        $price->update();

        return redirect('admin/deliverylocations/'.$delivery->parent_id)->with(['success'=>'deliveryLocations created successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('deliverylocation::show');
    }


    /**
     * @param DeliveryLocation $deliveryLocation
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $deliveryLocation = DeliveryLocation::find($id);
        $deliveryLocations = DeliveryLocation::getChildrenFlat(null,3);
        $parent = $deliveryLocation->parent_id;
        $branches = Branch::all()->pluck('name','id');
        $locations =  DeliveryLocation::getAll();
        $price = PriceDelivary::find(1);
        return view('deliverylocation::edit',compact('deliveryLocation','branches','price','locations','deliveryLocations','parent'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {

        $deliveryLocation = DeliveryLocation::find($id);
        $request->validate([
            'en.name'=>'required',
            'ar.name'=>'required',

        ]);
        $deliveryLocation->country_id      = $request->country_id;
        $deliveryLocation->city_id         = $request->city_id;
        $deliveryLocation->district_id     = $request->district_id;
        $deliveryLocation->branch_id       = $request->branch_id;
        $deliveryLocation->parent_id       = $request->parent_id;
        $deliveryLocation->update();
        $deliveryLocation->deleteTranslations();
        $deliveryLocation->translateAttributes($request->only('ar','en'));


        $price =  PriceDelivary::find(1);
        $price->min          = $request->min;
        $price->max          = $request->max;
        $price->price        = $request->price;
        $price->update();


        return redirect(url('admin/deliverylocations/'.$deliveryLocation->parent_id))->with(['success'=>'deliveryLocation Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $deliveryLocation = DeliveryLocation::find($id);

        $deliveryLocation->delete();
        $deliveryLocation->deleteTranslations();
        return redirect(url('admin/deliverylocations/'.$deliveryLocation->parent_id))->with(['success'=>'deliverylocations Deleted Successfully']);
    }

    public function datatable(Request $request)
    {
        $categories = DeliveryLocation::whereParentId($request->parent)->get();
        return \DataTables::of($categories)
            ->editColumn('name',function ($model){
                return "<a href='".url('admin/deliverylocations/'.$model->id)."'>".$model->name."</a>";
            })->editColumn('options',function ($model){
                return "<a href='".url('admin/deliverylocations/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/deliverylocations/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }


    public function calcolateMony($branch , $user)
    {

        if (Auth::check()) {

            $branch = DeliveryLocation::CalcolateDistance($branch , $user);

            $Price  = PriceDelivary::first();

            if($branch['distance']  > $Price->min && $branch['distance']  < $Price->max){
                return $Price->price;
            }


        }

    }



}
