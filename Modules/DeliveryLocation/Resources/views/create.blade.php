@extends('admin.layouts.master')
@section('page-title','Create DeliveryLocation')
@section('breadcrumb')
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    {{Form::open(['route'=>'store.delivery'])}}

    <div class="row">
    <div class="col-md-8">
       <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Create new Delivery Location</h2>
                </div>
            </div>
        </div>
        <div class="body">
            @include('admin.pratical.message')



            <ul class="nav nav-tabs">
                @foreach(locales() as $k=>$local)
                    <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                @endforeach
            </ul>

            <div class="tab-content">
                @foreach(locales() as $k=>$local)
                    <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                        <div class="form-group">
                            <label>Name {{ $local->code}} </label>
                            {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
                        </div>
                    </div>
                @endforeach

                    <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Country</label>
                                    {{Form::select('country_id',[],null,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>City</label>
                                    {{Form::select('city_id',[],null,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>District</label>
                                    {{Form::select('district_id',[],null,['class'=>'form-control'])}}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>District</label>
                                    {{Form::select('branch_id',$branches,null,['class'=>'form-control'])}}
                                </div>
                            </div>
                    </div>

                    {{Form::hidden('parent_id',$parent)}}
                <button type="submit" class="btn btn-success">Save</button>
            </div>




    </div>
    </div>
    </div>

    <div class="col-md-4">
            <div class="card">
                <div class="header">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Main Features Price</h2>
                        </div>
                    </div>
                </div>
                <div class="body">
                    <div class="form-group">
                        <label>Min Km</label>
                        {{Form::text('min',null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Max Km</label>
                        {{Form::text('max',null,['class'=>'form-control'])}}
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        {{Form::text('price',null,['class'=>'form-control'])}}
                    </div>

                </div>
            </div>

        </div>

    </div>
    {{Form::close()}}
@endsection

@section('scripts')
    <script>
        var allLocations    = @json($locations);
        var cities          = null;
        var districts       = null;
        fillOptionsFromList(allLocations,$('select[name="country_id"]'),{{old('country_id')}});

        $(document).ready(function () {
            cities      = getById(allLocations,{{old('country_id')}}).cities;
            fillOptionsFromList(cities,$('select[name="city_id"]'),{{old('city_id')}});

            districts = getById(cities,{{old('city_id')}}).districts;
            fillOptionsFromList(districts,$('select[name="district_id"]'),{{old('district_id')}});

        });

        $('select[name="country_id"]').on('change',function () {
            districts       = null;
            cities          = null;

            if($(this).val() === ""){
                districts   = null;
                cities      = null
            }else{
                cities      = getById(allLocations,$(this).val()).cities;
            }

            fillOptionsFromList(cities,$('select[name="city_id"]'));
            fillOptionsFromList(districts, $('select[name="district_id"]'));
        });

        $('select[name="city_id"]').on('change',function () {
            districts = getById(cities,$(this).val()).districts;
            fillOptionsFromList(districts,$('select[name="district_id"]'));
        });

        function getById($data,$id) {
            return getBy($data,'id',$id);
        }
        function getBy($data,$key,$value)
        {
            return $.grep( $data, function( n, i ) {
                return n['id']==$value;
            })[0];
        }

        function fillOptionsFromList($list,$slector,$selectedId)
        {
            $selectedId = $selectedId || 0;
            $slector.empty();
            if($list && $list.length != 0)
                $slector.append('<option value>Please Select</option>');

            $.each($list,function (items,$item) {
                selected = ($item.id===$selectedId)?"selected":"";
                $slector.append('<option value="'+$item.id+'" '+selected+' >'+$item.name+'</option>')
            });
        }

    </script>
@stop

