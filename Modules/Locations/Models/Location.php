<?php

namespace Modules\Locations\Models;

use App\Http\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use TranslatableTrait;
    protected $table        = 'locations';
    protected $fillable     = ['parent_id','code'];
    protected $appends      = ['name'];
    protected $translatable = ['name'];

    public function children()
    {
        return $this->hasMany(Location::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    public function parent()
    {
        $this->depth++;
        return $this->belongsTo(Location::class, 'parent_id');
    }

    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    public static function getAll()
    {
        $results = [];
        $countries = Location::where('parent_id',null)->get();
        foreach ($countries as $x=>$country){
            $results[$x]['id'] = $country->id;
            $results[$x]['name'] = $country->name;
            foreach ($country->children as $y=>$city){
                $results[$x]['cities'][$y]['id'] = $city->id;
                $results[$x]['cities'][$y]['name'] = $city->name;
                foreach ($city->children as $z=>$district) {
                    $results[$x]['cities'][$y]['districts'][$z]['id'] = $district->id;
                    $results[$x]['cities'][$y]['districts'][$z]['name'] = $district->name;
                }
            }
        }
        return $results;
    }

    public function getNameAttribute()
    {
        return $this->en['name'];
    }
}

