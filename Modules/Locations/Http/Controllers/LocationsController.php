<?php

namespace Modules\Locations\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Locations\Models\Location;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($parent = null)
    {
        return view('locations::index',compact('parent'));
    }



    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($parent = null)
    {
        $parentLocation = null;
        if($parent != null )
            $parentLocation = Location::findOrFail($parent);

        return view('locations::create',compact('parentLocation','parent'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'ar.name'=>'required',
            'en.name'=>'required'
        ]);
        $location = new Location();
        $location->parent_id = $request->parent_id;
        $location->save();
        $location->translateAttributes($request->all());
        return redirect('admin/locations/'.$location->parent_id)->with(['success'=>'Location created Successfully']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('locations::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Location $location)
    {
        return view('locations::edit',compact('location'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Location $location,Request $request)
    {
        $this->validate($request,[
            'ar.name'=>'required',
            'en.name'=>'required'
        ]);
        $location->translateAttributes($request->all());
        return redirect('admin/locations/'.$location->parent_id)->with(['success'=>'Location updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
        $location->deleteTranslations();
        return redirect('admin/locations/'.$location->parent_id)->with(['success'=>'Location deleted Successfully']);
    }

    public function datatable($parent = null)
    {
        $locations = Location::where('parent_id',$parent)->get();
        return \DataTables::of($locations)
            ->editColumn('name',function ($model) use($parent) {
                return "<a href='".url('admin/locations/'.$model->id)."'>".$model->name."</a>";
            })->editColumn('parent',function ($model) use($parent) {
                if(!$parent)
                    return "";
                return "<a href='".url('admin/locations/'.$parent)."'>".$model->parent->name."</a>";
            })->editColumn('options',function ($model){
                return "<a href='".url('admin/locations/'.$model->id.'/edit')."' class='btn btn-warning'>Edit</a>
                <a href='".url('admin/locations/'.$model->id.'/delete')."' class='btn btn-danger'>Delete</a>";

            })
            ->make(true);
    }


}
